Variable name will follow camelCase.
DAL: Data Access Layer. It will store Data interaction script
BAL: Business Access Layer: It will store Business logic script
Ajax: It will store script that deals with ajax request/response.
Admin: It will store script that deals with admin area.
Bootstrap: It contains the folders and file used for twitter bootstrap template.
fpdf: It's a library required to generate pdf from provided content.
Image: It will store question, option & other images.
js: It will store javascript.
style: It will contain css file for the application.
upload: It will temporarily store the uploaded files. Mainly used to store excel files for bulk upload.

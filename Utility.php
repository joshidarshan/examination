<?php

function GetCurrentTime() {
    date_default_timezone_set('Asia/Calcutta');
    $date = date('m/d/Y h:i:s', time());
    return $date;
}

function GetCurrentDate(){
    date_default_timezone_set('Asia/Calcutta');
    $date = date('m/d/Y', time());
    return $date;
}

?>

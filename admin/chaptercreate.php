<?php 
    if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'chapter';
    
    include_once '../dbUtility/Class.php';
    include_once '../dbUtility/Subject.php';
    include_once '../dbUtility/Chapter.php';
    
    $std = GetAllClasses();
    $sub = GetAllSubjects();    
    
    if(isset($_POST) && !empty($_POST)){
        $standard = $_POST['standard'];
        $subject = $_POST['subject'];
        $chapter = $_POST['chapter'];
        
        $chpId = CreateChapter($chapter, $standard, $subject);
    }
    
    include_once '../templets/adminHeaderNew.php';
?>

<form action="chaptercreate.php" method="POST">
    <div class="contaner">
        <div class="contaner_top-adminuserlisting">
            <div class="subject-adminuserlisting">
                <h3>New Chapter</h3>
            </div>
        </div>
        <!-- style="width: 90%; margin-left: 50px;" -->
        <table class="table table-striped">
            <tr>
                <td>Standard</td>
                <td>
                    <select id="standard" class="dropdown" name="standard">
                        <?php
                            while($row = mysql_fetch_assoc($std)){
                                echo "<option vlaue=$row[Id]>$row[Name]</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Subject</td>
                <td>
                    <select id="subject" class="dropdown" name="subject">
                        <?php
                        while ($row = mysql_fetch_assoc($sub)){
                            echo "<option value=$row[Id]>$row[Name]</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Chapter</td>
                <td><input type="text" class="text" name="chapter" id="chapter" /></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <input type="submit" name="submit" value="Submit" class="btn btn-large" />
                </td>
            </tr>
        </table>
        
    </div>
</form>
<?php include_once '../templets/footerTemplate.php'; ?>
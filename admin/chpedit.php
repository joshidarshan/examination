<?php
if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'chapter';

include_once '../dbUtility/Class.php';
include_once '../dbUtility/Chapter.php';
include_once '../dbUtility/Subject.php';

$classList = GetAllClasses();
$subList = GetAllSubjects();
$chpQuery = GetChapter($_GET['cid']);
$chpData = mysql_fetch_assoc($chpQuery);

// Handling post back
if(!empty($_POST) && isset($_POST)){
    $chpId = $_POST['cid'];
    $chapterName = $_POST['chapter'];
    $stdId = $_POST['standard'];
    $subid = $_POST['subject'];
    $status = $_POST['rbtStatus'];
    
    $updateChp = SetChapter($chpId, $chapterName, $stdId, $subid, $status);
    header('location: chplisting.php');
}
include_once '../templets/adminHeaderNew.php';
?>

<form action="chpedit.php" method="POST">
    <div>
        <h3>Edit Chapter</h3>
    </div>
<table class="table table-striped" style="margin-top: 35px;">
    <tr>
        <td>Name</td>
        <td><input type="text" id="chapter" name="chapter" value="<?php echo $chpData['Name']; ?>" /></td>
    </tr>
    <tr>
        <td>Subject</td>
        <td>
            <select id="subject" name="subject">
                <?php
                    while($row = mysql_fetch_assoc($subList)){
                        if($row['Id'] == $chpData['subId'])
                            echo "<option value='$row[Id]' selected='selected'>$row[Name]</option>";
                        else 
                            echo "<option value='$row[Id]'>$row[Name]</option>";
                    }
                    unset($row);
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td>Standard</td>
        <td>
            <select id="standard" name="standard">
                <?php
                    while($row = mysql_fetch_assoc($classList)){
                        if($row['Id'] == $chpData['stdId'])
                            echo "<option value='$row[Id]' selected='selected'>$row[Name]</option>";
                        else
                            echo "<option value='$row[Id]'>$row[Name]</option>";
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td>Status</td>
        <td>
            <input type="radio" class="radio" name="rbtStatus" <?php if($chpData['Status'] == '1') echo "checked=checked" ?> value="1">Active <br />
            <input type="radio" class="radio" name="rbtStatus" <?php if($chpData['Status'] == '0') echo "checked=checked" ?> value="0">Inactive
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Submit" class="btn btn-large" /></td>
    </tr>
</table>
    <input type="hidden" id="cid" name="cid" value="<?php echo $_GET['cid']; ?>" />
</form>

<?php
include_once '../templets/footerTemplate.php';
?>
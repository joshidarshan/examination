<?php 
    if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'chapter';
    
    include_once '../dbUtility/Chapter.php';
    include_once '../templets/adminHeaderNew.php';
    
    $chpData = GetChapterListing();
?>

<form action="userlisting.php" method="POST">
    <div class="contaner">
        <div class="contaner_top-adminuserlisting">
            <div class="subject-adminuserlisting">
                <h3>Class Listing</h3>
            </div>
        </div>
        <!-- style="width: 90%; margin-left: 50px;" -->
        <table class="table table-striped">
            <tr style='text-align: left;'>
                <th>Action</th>
                <th>Id</th>
                <th>Name</th>
                <th>Subject</th>
                <th>Standard</th>
                <th>Status</th>
            </tr>
            <?php
                while($row = mysql_fetch_assoc($chpData)){
                    echo "<tr>";
                    echo "<td style='text-align: left; width:150px;'>
                            <a href='chpedit.php?cid=$row[chid]' style='margin-right: 10px;'>Edit</a>
                            <a href='chpdetail.php?cid=$row[chid]' style='margin-right: 10px;'>Select</a>
                            <a href='chpdelete.php?cid=$row[chid]' style='margin-right: 10px;'>Delete</a>
                        </td>";
                    echo "<td style='text-align: left; width:150px;'>$row[chid]</td>";
                    echo "<td style='text-align: left; width:150px;'>$row[chname]</td>";
                    echo "<td style='text-align: left; width:150px;'>$row[sbname]</td>";
                    echo "<td style='text-align: left; width:150px;'>$row[clname]</td>";
                    
                    if($row['Status'] == 1)
                        echo "<td style='text-align: left; width:150px;'>Active</td>";
                    else
                        echo "<td style='text-align: left; width:150px;'>Deactive</td>";
                    
                    echo "</tr>";
                }
            ?>
        </table>
        
    </div>
</form>
<?php include_once '../templets/footerTemplate.php'; ?>
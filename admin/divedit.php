<?php
if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'class';

include_once '../dbUtility/Class.php';
include_once '../dbUtility/division.php';

$classList = GetAllClasses();
$divQuery = GetClassandDivision($_GET['did']);
$divData = mysql_fetch_assoc($divQuery);

// Handling post back
if(!empty($_POST) && isset($_POST)){
    $classId = $_POST['class'];
    $division = $_POST['division'];
    $divId = $_POST['did'];
    $status = $_POST['rbtStatus'];
    
    SetDivision($divId, $division, $classId, $status);
    header('location: divlisting.php');
}
include_once '../templets/adminHeaderNew.php';
?>

<form action="stdedit.php" method="POST">
    <div>
        <h3>Edit Division</h3>
    </div>
<table class="table table-striped" style="margin-top: 35px;">
    <tr>
        <td>Class</td>
        <td>
            <select id="class" name="class">
                <?php
                    while($row = mysql_fetch_assoc($classList)){
                        if($row['Id'] == $divData['clid'])
                            echo "<option value=$row[Id] selected='selected'>$row[Name]</option>";
                        else
                            echo "<option value=$row[Id]>$row[Name]</option>";
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td>Division</td>
        <td><input type="text" id="division" name="division" value="<?php echo $divData['diname']; ?>" /></td>
    </tr>
    <tr>
        <td>Status: </td>
        <td>
            <input type="radio" class="radio" name="rbtStatus" <?php if($divData['Status'] == '1') echo "checked=checked" ?> value="1">Active <br />
            <input type="radio" class="radio" name="rbtStatus" <?php if($divData['Status'] == '0') echo "checked=checked" ?> value="0">De-active
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Submit" class="btn btn-large" /></td>
    </tr>
</table>
    <input type="hidden" id="id" name="did" value="<?php echo $_GET['did']; ?>" />
</form>

<?php
include_once '../templets/footerTemplate.php';
?>
<?php 
    if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'class';
    
    include_once '../dbUtility/Class.php';
    include_once '../dbUtility/division.php';
    $allStd = GetAllClasses();
    
    if(isset($_POST) && !empty($_POST)){
        $div = $_POST['div'];
        $std = $_POST['std'];
        $divId = CreateDivision($div, $std);
        /*if(isset($divId) && !empty($divId))
            echo "<script>alert('Division: $divId Created')</script>";*/
    }
    
    include_once '../templets/adminHeaderNew.php';
?>

<form action="divisioncreate.php" method="POST">
    <div class="contaner">
        <div class="contaner_top-adminuserlisting">
            <div class="subject-adminuserlisting">
                <h3>New Division</h3>
            </div>
        </div>
        <!-- style="width: 90%; margin-left: 50px;" -->
        <table class="table table-striped">
            <tr>
                <td>Standard</td>
                <td>
                    <select id="std" name="std">
                        <?php
                            while($row = mysql_fetch_assoc($allStd)){
                                echo "<option value=$row[Id]>$row[Name]</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Division</td>
                <td><input type="text" class="text" name="div" id="div" /></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <input type="submit" name="submit" value="Submit" class="btn btn-large" />
                </td>
            </tr>
        </table>
        
    </div>
</form>
<?php include_once '../templets/footerTemplate.php'; ?>
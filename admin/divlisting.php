<?php 
    if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'class';
    
    include_once '../templets/adminHeaderNew.php';
    include_once '../dbUtility/Class.php';
    include_once '../dbUtility/division.php';
    
    $div = GetAllClassDivision();
    
?>

<form action="userlisting.php" method="POST">
    <div class="contaner">
        <div class="contaner_top-adminuserlisting">
            <div class="subject-adminuserlisting">
                <h3>Division Listing</h3>
            </div>
        </div>
        <!-- style="width: 90%; margin-left: 50px;" -->
        <table class="table table-striped">
            <tr style='text-align: left;'>
                <th>Action</th>
                <th>Id</th>
                <th>Class - Division</th>
                <th>Status</th>
            </tr>
            <?php
                while($row = mysql_fetch_assoc($div)){
                    //SELECT cl.`Id` as clid, cl.`Name` as clname, di.`Id` as did, di.`Name` as diname
                    echo "<tr>";
                    echo "<td style='text-align: left; width:150px;'>
                            <a href='divedit.php?did=$row[did]' style='margin-right: 10px;'>Edit</a>
                            <a href='divdetail.php?did=$row[did]' style='margin-right: 10px;'>Select</a>
                            <a href='divdelete.php?did=$row[did]' style='margin-right: 10px;'>Delete</a>
                        </td>";
                    echo "<td style='text-align: left; width:150px;'>$row[did]</td>";
                    echo "<td style='text-align: left; width:150px;'>".$row['clname']." - ".$row['diname']."</td>";
                    
                    if($row['Status'] == 1)
                        echo "<td style='text-align: left; width:150px;'>Active</td>";
                    else
                        echo "<td style='text-align: left; width:150px;'>Deactive</td>";
                    
                    echo "</tr>";
                }
            ?>
        </table>
        
    </div>
</form>
<?php include_once '../templets/footerTemplate.php'; ?>
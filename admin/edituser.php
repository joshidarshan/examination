<?php
if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'user';

//include_once '../templets/adminTemplateHeader.php';
include_once '../dbUtility/User.php';
include_once '../dbUtility/Class.php';
include_once '../dbUtility/division.php';

// Handling post back
if(!empty($_POST) && isset($_POST)){
    $uid = $_POST['uid'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $role = $_POST['role'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $class = $_POST['class'];
    $division = $_POST['division'];
    $status = $_POST['rbtStatus'];
    
    $isUpdated = SetUser($uid, $username, $password, $role, $firstname, $lastname, $class, $division, $status);
    if($isUpdated){
        echo "<script>alert('User updated successfuly')</script>";
        header('Location: userlisting.php');
    }
    else{
        echo "<script>alert('User updated successfuly')</script>";
        header('Location: edituser.php?uid='.$_GET['uid']);
    }
}
include_once '../templets/adminHeaderNew.php';
$userId = $_GET['uid'];
$userDataTemp = getUserInfo($userId);
$userData = mysql_fetch_assoc($userDataTemp);
$std = GetAllClasses();
$div = GetAllDivisions();

?>

<div class="notifications top-left"></div>
<script>
$('.top-left').notify({
    message: { text: "User modified!" }
}).show();
</script>

<form action="edituser.php" method="POST">
    <div>
        <h3>Edit User: <?php echo $userData['FirstName']." ".$userData['LastName']; ?></h3>
    </div>
<table class="table table-striped" style="margin-top: 35px;">
    <tr>
        <td>Username</td>
        <td><?php echo "<input type='textbox' name='username' value=".$userData['UserName']." />"; ?></td>
    </tr>
    <tr>
        <td>Password</td>
        <td><?php echo "<input type='textbox' name='password' value=".$userData['Password']." />"; ?></td>
    </tr>
    <tr>
        <td>Role</td>
        <td>
            <select id="role" name="role">
                <option value="Student" <?php if($userData['Role'] == 'Student') echo "selected=selected"; ?> >Student</option>
                <option value="Admin" <?php if($userData['Role'] == 'Admin') echo "selected=selected"; ?> >Admin</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>FirstName</td>
        <td><?php echo "<input type='textbox' name='firstname' value=".$userData['FirstName']." />";?></td>
    </tr>
    <tr>
        <td>LastName</td>
        <td><?php echo "<input type='textbox' name='lastname' value=".$userData['LastName']." />"; ?></td>
    </tr>
    <tr>
        <td>Class</td>
        <td>
            <select id="class" name="class">
            <?php
//SELECT us.UserName, us.Password, us.Role, us.FirstName, us.LastName, cl.Name, di.`Name`, us.`ClassId` as classId, us.`DivId` as divId
                while($cr = mysql_fetch_assoc($std)){
                    if($cr['Id'] == $userData['classId'])
                        echo "<option selected='selected' value=$cr[Id]>$cr[Name]</option>";
                    else
                        echo "<option value=$cr[Id]>$cr[Name]</option>";
                }
            ?>
            </select>
        </td>
    </tr>
    <tr>
        <td>Division</td>
        <td>
            <select id="division" name="division">
                <?php
                    while($dr = mysql_fetch_assoc($div)){
                        if($dr['Id'] == $userData['divId'])
                            echo "<option selected='selected' value=$dr[Id]>$dr[Name]</option>";
                        else
                            echo "<option value=$dr[Id]>$dr[Name]</option>";
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td>Status: <?php echo $userData['Status']; ?></td>
        <td>
            <input type="radio" class="radio" name="rbtStatus" <?php if($userData['Status'] == '1') echo "checked=checked" ?> value="1">Active <br />
            <input type="radio" class="radio" name="rbtStatus" <?php if($userData['Status'] == '0') echo "checked=checked" ?> value="0">Inactive
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Submit" class="btn btn-large" /></td>
    </tr>
</table>
    <input type="hidden" id="id" name="uid" value="<?php echo $_GET['uid']; ?>" />
</form>

<?php
include_once '../templets/footerTemplate.php';
?>
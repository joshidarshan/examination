<?php
if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'test';

include_once '../dbUtility/Class.php';
include_once '../dbUtility/exam.php';

$classList = GetAllClasses();
$examQuery = GetEditExamDetail($_GET['eid']);
$examData = mysql_fetch_assoc($examQuery);

// Handling post back
if(!empty($_POST) && isset($_POST)){
    //$examId, $examName, $classId, $startFrom, $endTo, $timeLimit, $posMark, $nagMark, $uid, $status
    $examId = $_POST['eid'];
    $examName = $_POST['exam'];
    $classId = $_POST['class'];
    $startFrom = $_POST['startfrom'];
    $endTo = $_POST['endto'];
    $timeLimit = $_POST['timelimit'];
    $posMark = $_POST['positivemark'];
    $negMark = $_POST['negativemark'];
    $uid = $_SESSION['UID'];
    $status = $_POST['rbtStatus'];
    
    SetExam($examId, $examName, $classId, $startFrom, $endTo, $timeLimit, $posMark, $negMark, $uid, $status);
    header('location: examlisting.php');
}
include_once '../templets/adminHeaderNew.php';
?>

<form action="examedit.php" method="POST">
    <div>
        <h3>Edit Division</h3>
    </div>
<table class="table table-striped" style="margin-top: 35px;">
    <tr>
        <td>Exam</td>
        <td><input type="text" id="exam" name="exam" value="<?php echo $examData['Name']; ?>" /></td>
    </tr>
    <tr>
        <td>Start From</td>
        <td><input type="text" id="startfrom" name="startfrom" value="<?php echo $examData['startFrom']; ?>" /></td>
    </tr>
    <tr>
        <td>End To</td>
        <td><input type="text" id="endto" name="endto" value="<?php echo $examData['endTo']; ?>" /></td>
    </tr>
    <tr>
        <td>Time Limit</td>
        <td><input type="text" id="timelimit" name="timelimit" value="<?php echo $examData['timeLimit']; ?>" /></td>
    </tr>
    <tr>
        <td>Positive Mark</td>
        <td><input type="text" id="positivemark" name="positivemark" value="<?php echo $examData['positiveMark']; ?>" /></td>
    </tr>
    <tr>
        <td>Negative Mark</td>
        <td><input type="text" id="negativemark" name="negativemark" value="<?php echo $examData['negativeMark']; ?>" /></td>
    </tr>
    <tr>
        <td>Class</td>
        <td>
            <select id="class" name="class">
                <?php
                    while($row = mysql_fetch_assoc($classList)){
                        if($row['Id'] == $examData['classId'])
                            echo "<option value=$row[Id] selected='selected'>$row[Name]</option>";
                        else
                            echo "<option value=$row[Id]>$row[Name]</option>";
                    }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td>Status</td>
        <td>
            <input type="radio" class="radio" name="rbtStatus" <?php if($examData['status'] == '1') echo "checked=checked" ?> value="1">Active <br />
            <input type="radio" class="radio" name="rbtStatus" <?php if($examData['status'] == '0') echo "checked=checked" ?> value="0">De-active
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Submit" class="btn btn-large" /></td>
    </tr>
</table>
    <input type="hidden" id="id" name="eid" value="<?php echo $_GET['eid']; ?>" />
</form>

<?php
include_once '../templets/footerTemplate.php';
?>
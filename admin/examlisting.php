<?php 
    if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'test';
    
    include_once '../dbUtility/exam.php';
    include_once '../templets/adminHeaderNew.php';
    
    $examData = "";
    if(isset($_POST['std']) && !empty($_POST['std'])){
        $examData = GetStdnameExam($_POST['std']);
        
        include_once '../dbUtility/Class.php';
        $stdList = GetAllClasses();
    }
?>

<form action="examlisting.php" method="POST">
    <div class="contaner">
        <div class="contaner_top-adminuserlisting">
            <div class="subject-adminuserlisting">
                <h3>Exam Listing</h3>
            </div>
        </div>
        <!-- style="width: 90%; margin-left: 50px;" -->
        <?php
            if(!empty($examData)){
        ?>
        <table class="table table-striped">
            <tr style='text-align: left;'>
                <th>Action</th>
                <th>Id</th>
                <th>Name</th>
                <th>Standard</th>
                <th>Status</th>
            </tr>
            <?php
        //SELECT `Id`, `Name`, `classId`, `startFrom`, `endTo`, `timeLimit`, `positiveMark`, `negativeMark`, status
                while($row = mysql_fetch_assoc($examData)){
                    echo "<tr>";
                    echo "<td style='text-align: left; width:150px;'>
                            <a href='examedit.php?eid=$row[Id]' style='margin-right: 10px;'>Edit</a>
                            <a href='examdetail.php?eid=$row[Id]' style='margin-right: 10px;'>Select</a>
                            <a href='examdelete.php?eid=$row[Id]' style='margin-right: 10px;'>Delete</a>
                        </td>";
                    echo "<td style='text-align: left; width:150px;'>$row[Id]</td>";
                    echo "<td style='text-align: left; width:150px;'>$row[Name]</td>";
                    echo "<td style='text-align: left; width:150px;'>$row[clname]</td>";
                    
                    if($row['Status'] == 1)
                        echo "<td style='text-align: left; width:150px;'>Active</td>";
                    else
                        echo "<td style='text-align: left; width:150px;'>Inactive</td>";
                    
                    echo "</tr>";
                }
            ?>
        </table>
        <?php
            }
            else{
        ?>
        <table class="table table-striped">
            <tr>
                <td>Standard</td>
                <td>
                    <select id="std" name="std">
                        <?php
                            include_once '../dbUtility/Class.php';
                            $std = GetAllClasses();
                            while($row = mysql_fetch_assoc($std)){
                                echo "<option value=$row[Id]>$row[Name]</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center"><input type="submit" name="submit" value="Submit" class="btn btn-large" /></td>
            </tr>
        </table>
            <?php } ?>
        
    </div>
</form>
<?php include_once '../templets/footerTemplate.php'; ?>
<?php
 set_time_limit(600);
include_once '../templets/adminTemplateHeader.php';
include_once '../dbUtility/Class.php';
include_once '../dbUtility/Subject.php';

if(!empty($_GET['t'])){
    if($_GET['t'] == 'success')
        echo "<script>alert('Exam generation successful.')</script>";
}

if (!empty($_POST)) {

    // Inserting exam recored
    include_once '../dbUtility/exam.php';


    $allSubjectsQuery = GetAllSubjects();
    $allSubjects = array();
    while ($row = mysql_fetch_assoc($allSubjectsQuery)) {
        $local_array['Id'] = $row['Id'];
        $local_array['Name'] = $row['Name'];

        array_push($allSubjects, $local_array);
    }

    $examName = $_POST['examName'];
    $stdId = $_POST['selStd'];
    $complexityLevel = $_POST['selComplex'];
    $positiveMarks = $_POST['positiveMarks'];
    $negativeMarks = $_POST['negativeMarks'];
    $time = $_POST['time'];
    $validFrom = $_POST['validFrom'];
    $validTo = $_POST['validTo'];
    $uid = $_SESSION['UID'];

    Connect();
    
    $lastExamIdSql = "SELECT Id
                            FROM exams
                            WHERE `classId` = '$stdId' AND `status` = '1'
                            ORDER BY Id DESC
                            LIMIT 0, 1";
    $lastExamIdQuery = mysql_query($lastExamIdSql);
    $lastExamIdData = mysql_fetch_assoc($lastExamIdQuery);
    $lastExamId = $lastExamIdData['Id'];
    
    $lastExamQidSql = "SELECT `questionId`
                            FROM examquestions
                            WHERE `examId` = '$lastExamId'";
    $lastExamQidQuery = mysql_query($lastExamQidSql);
    $lastExamData = array();
    $lastExamQidString = "";
    while ($row = mysql_fetch_assoc($lastExamQidQuery)) {
        $lastExamData[] = $row['questionId'];
    }
    for ($i = 0; $i < count($lastExamData); $i++) {
        if ($i == (count($lastExamData) - 1))
            $lastExamQidString .= $lastExamData[$i];
        else
            $lastExamQidString .= $lastExamData[$i] . ",";
    }
    Disconnect();
    
    $examId = CreateExam($examName, $stdId, $positiveMarks, $negativeMarks, $time, $validFrom, $validTo, $uid);
     
    $totalQuestion = 0;
    if (isset($examId) && $examId > 0) {
        Connect();
        // Getting question list for previous exam.

        

        // geting list of subjects selected for currect exam
        $subIdUsed = array();
        $subIdString = "";
        foreach ($allSubjects as $key => $value) {
            $subjectName = $value['Name'];
            $subjectName = str_replace(" ", "_", $subjectName);
            $subjectId = $value['Id'];

            if (isset($_POST[$subjectName]) && !empty($_POST[$subjectName])) {
                $subIdUsed[] = $subjectId;
            }
        }
        //
        //echo $subIdString;
        
        for ($i = 0; $i < count($subIdUsed); $i++) {
            if ($i == count($subIdUsed) - 1)
                $subIdString .= $subIdUsed[$i];
            else
                $subIdString .= $subIdUsed[$i] . ", ";
        }

        /*if(empty($subIdString))
            header('Location: generateexam.php?t=success');*/
        
        // getting count of qids
        $selGetCountQidSql = "SELECT COUNT(`Id`) as CT
                                FROM questions";
        $selGetCountQidQuery = mysql_query($selGetCountQidSql);
        $selGetCountQidData = mysql_fetch_assoc($selGetCountQidQuery);
        $countQid = $selGetCountQidData['CT'];

        // getting list of questionid, subid, complexity for questions.  
        $qidListForExamSql = "";
        if(empty($lastExamQidString)){
            $qidListForExamSql = "SELECT `Id`, `subId`, `complexity`
                FROM questions
                WHERE subId IN ( $subIdString )
                ORDER BY `subId`
                LIMIT 0 , $countQid";
            
        }
        else{
        $qidListForExamSql = "SELECT `Id`, `subId`, `complexity`
                FROM questions
                WHERE subId IN ( $subIdString ) AND `Id` NOT IN ( $lastExamQidString )
                ORDER BY `subId`
                LIMIT 0 , $countQid";
        }
        //echo $qidListForExamSql;
        $qidListForExamQuery = mysql_query($qidListForExamSql);
        $qidList = array();
        while ($row = mysql_fetch_assoc($qidListForExamQuery)) {
            $local_arr['qid'] = $row['Id'];
            $local_arr['subId'] = $row['subId'];
            $local_arr['complexity'] = $row['complexity'];

            array_push($qidList, $local_arr);
        }
        shuffle($qidList);

        // array that contains the qid to be pushed to exam
        $finalQid = array();

        // subject wise tough, medium, easy count.
        $tough = 0; $medium = 0; $easy = 0;
        foreach ($allSubjects as $key => $value) {
            $subjectName = $value['Name'];
            $subjectName = str_replace(" ", "_", $subjectName);
            $subjectId = $value['Id'];
            

            if (isset($_POST[$subjectName]) && !empty($_POST[$subjectName])) {
                $subjectQuestionCount = $_POST[$subjectName];
                $noQue = ceil($subjectQuestionCount);
                $totalQuestion += $noQue;

                // Complexity: 3
                if ($complexityLevel == "3") {
                    $tough = round(($noQue * 50) / 100);
                    $medium = round(($noQue * 30) / 100);
                    $easy = round(($noQue * 20) / 100);
                    
                    $tot = $tough+$medium+$easy;
                    if($tot > $noQue)
                        $tough -= $tot-$noQue;
                }

                // Complexity: 2
                else if ($complexityLevel == "2") {
                    $tough = round(($noQue * 20) / 100);
                    $medium = round(($noQue * 50) / 100);
                    $easy = round(($noQue * 30) / 100);
                    
                    $tot = $tough+$medium+$easy;
                    if($tot > $noQue)
                        $medium -= $tot-$noQue;
                }

                // Complexity: 1
                else if ($complexityLevel == "1") {
                    $tough = round(($noQue * 20) / 100);
                    $medium = round(($noQue * 30) / 100);
                    $easy = round(($noQue * 50) / 100);
                    
                    $tot = $tough+$medium+$easy;
                    if($tot > $noQue)
                        $easy -= $tot-$noQue;
                }

                // according to subId, complexity filling qid array.
                foreach ($qidList as $key => $value) {
                    $qi = $value['qid'];
                    if ($value['subId'] == $subjectId) {
                        if ($tough > 0) {
                            if ($value['complexity'] == '3') {
                                $finalQid[] = $qi;
                                $tough--;
                            }
                        }
                        if ($medium > 0) {
                            if ($value['complexity'] == '2') {
                                $finalQid[] = $qi;
                                $medium--;
                            }
                        }
                        if ($easy > 0) {
                            if ($value['complexity'] == '1') {
                                $finalQid[] = $qi;
                                $easy--;
                            }
                        }
                        
                    }
                }
                if(count($finalQid) < $totalQuestion){
                    $remain = $totalQuestion - count($finalQid);
                    //echo "Remain: $remain";
                    for($i=0;$i<$remain;$i++){
                        if($i < count($lastExamData))
                        $finalQid[] = $lastExamData[$i];
                    }
                }
            }
        }
        Disconnect();
        // inserting question for exam
        include_once '../dbUtility/Question.php';
        foreach ($finalQid as $key=>$value){
            InsertExamQuestion($examId, $value);
        }
    }
}
?>

<div class="contaner">
    <div class="contaner_top-adminnewuser">
        <form action="generateexam.php" method="POST">
            <table width="100%" border="1" style="border: 1px solid black; margin-top: 10px; text-align: left;">
                <tr>
                    <td style="width: 200px;">Exam Name</td>
                    <td><input type="text" id="examName" name="examName" /> </td>
                </tr>
                <tr>
                    <td>Standard</td>
                    <td>
                        <select id="selStd" name="selStd">
                            <?php
                            $std = GetAllClasses();
                            if (isset($std) && !empty($std)) {
                                while ($row = mysql_fetch_assoc($std)) {
                                    echo "<option value=" . $row['Id'] . ">" . $row['Name'] . "</option>";
                                }
                            }
                            ?>
                        </select> 
                    </td>
                </tr>

                <tr>
                    <td>Subjects</td>
                    <td style="line-height: 30px;">
                        <div style="max-height: 300px; overflow: scroll;">
                            <?php
                            $sub = GetAllSubjects();
                            $count = 0;
                            if (isset($sub) && !empty($std)) {
                                echo "<table>";
                                while ($row = mysql_fetch_assoc($sub)) {
                                    //echo "<input id='sub-$row[Id]' type='checkbox' value=".$row['Id']." onclick='return SubjectChange($row[Id]);' style='float: left;'/><label for='sub-$row[Id]' style='margin-left: 10px;'>".$row['Name']."</label><br />";
                                    echo "<tr>";
                                    echo "<td><input id='sub-$row[Id]' type='checkbox' value=" . $row['Id'] . " style='float: left;'/></td><td><label for='sub-$row[Id]' style='margin-left: 10px;'>" . $row['Name'] . "</label></td><td> <input type='text' name='$row[Name]' onblur='return fillVal();' /></td>";
                                    echo "</tr>";
                                    $count++;
                                }
                                echo "</table>";
                            }
                            ?>
                        </div>
                    </td>
                </tr>

<!--<tr>
    <td>Chapters</td>
    <td>
        <div id="chapters" style="max-height: 300px; overflow: scroll;">
            
        </div>
    </td>
</tr>-->

                <tr>
                    <td>Complexity Level</td>
                    <td>
                        <select id="selComplex" name="selComplex">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>Positive Marks</td>
                    <td><input type="text" id="positiveMarks" name="positiveMarks" value="1" /></td>
                </tr>

                <tr>
                    <td>Negative Marks</td>
                    <td><input type="text" id="negativeMarks" name="negativeMarks" value="-0.25" /></td>
                </tr>

                <tr>
                    <td>Time for exam</td>
                    <td><input type="text" id="time" name="time" placeholder="Time in minutes" /></td>
                </tr>

                <tr>
                    <td>Valid From</td>
                    <td><input type="text" id="validFrom" name="validFrom" class="cal"></td>
                </tr>

                <tr>
                    <td>Valid To</td>
                    <td><input type="text" id="validTo" name="validTo" class="cal"/></td>
                </tr>

                <tr>
                    <td colspan="2"><input type="Submit" name="Submit" value="Submit" /></td>
                </tr>
            </table>
        </form>  
    </div>
</div>
<input type="hidden" id="subque" name="subque" value="0">

<script>
    function SubjectChange(id) {
        isSel = false;
        if ($("#sub-" + id).is(":checked"))
            isSel = true;

        stdId = $("#selStd").val();
        subId = id;
        if (isSel) {
            $.ajax({
                type: "GET",
                url: "../utility/ajaxchapter.php",
                data: "stdId=" + stdId + "&subId=" + subId + "&type=chk",
                success: function(data) {
                    alert(data);
                    $("#chapters").append(data);
                }
            });
        }
        else {
            $("." + id).remove();
        }
    }
</script>

<?php
include_once '../templets/footerTemplate.php';
?>

<?php 
if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'question';
    
header('Content-type: text/html; charset=utf-8');

//include_once '../templets/adminTemplateHeader.php';
include_once '../dbUtility/Question.php';
include_once '../dbUtility/Class.php';
include_once '../dbUtility/Subject.php';
include_once '../dbUtility/Option.php';
include_once '../dbUtility/exam.php';

if(!empty($_GET['t'])){
    if($_GET['t'] == 'success')
        echo "<script>alert('Question insertion successful.')</script>";
}

if(!empty($_POST)){
    $std = $_POST['std'];
    $exam = $_POST['exam'];
    $sub = $_POST['sub'];
    $chp = $_POST['chp'];
    $question = $_POST['question'];
    $opt1 = $_POST['opt1'];
    $opt2 = $_POST['opt2'];
    $opt3 = $_POST['opt3'];
    $opt4 = $_POST['opt4'];
    $language = $_POST['lan'];
    $complexity = $_POST['complexity'];
    $correctOption = $_POST['opt'];
    $positiveMarks = $_POST['positivemarks'];
    $negativeMarks = $_POST['negativemarks'];
    
    if($chp == 'select')
        $chp=0;
    if(empty($chp) || !isset($chp))
            $chp=0;
        
    if($exam == 'select')
        $exam=0;
    if(empty($exam) || !isset($exam))
            $exam=0;
        
    $qid = InsertQuestion($question, $sub, $chp, $complexity, $language, $positiveMarks, $negativeMarks);
    
    if($qid > 0){
        
        
        
        if(!empty($opt1)){
            $isCorrect = 0;
            foreach($correctOption as $key=>$value){
                if($value == 1){
                    $isCorrect = 1;
                    break;
                }
            }
            $oid = InsertOption($qid, $opt1, $isCorrect);
        }
        if(!empty($opt2)){
            $isCorrect = 0;
            foreach($correctOption as $key=>$value){
                if($value == 2){
                    $isCorrect = 1;
                    break;
                }
            }
            $oid = InsertOption($qid, $opt2, $isCorrect);
        }
        if(!empty($opt3)){
            $isCorrect = 0;
            foreach($correctOption as $key=>$value){
                if($value == 3){
                    $isCorrect = 1;
                    break;
                }
            }
            $oid = InsertOption($qid, $opt3, $isCorrect);
        }
        if(!empty($opt4)){
            $isCorrect = 0;
            foreach($correctOption as $key=>$value){
                if($value == 4){
                    $isCorrect = 1;
                    break;
                }
            }
            $oid = InsertOption($qid, $opt4, $isCorrect);
        }
        if(isset($exam) && !empty($exam) && $exam != 0)
            $eq = InsertExamQuestion($exam, $qid);
    }
    
    
    /*$qid = InsertQuestion($question, $chp, $complexity);
    for($i=0;$i<4;$i++){
        $isCorrect = 0;
        $option = $_POST['opt'.$i];
        $correctOptionId = $_POST['opt'];
        if($i+1 == $correctOptionId)
            $isCorrect = 1;
        $oid = InsertOption($qid, $option, $isCorrect);
    }*/
    
    header('Location: insertquestion.php?t=success');
}
include_once '../templets/adminHeaderNew.php';
?>

<script src="../tiny_mce/tiny_mce.js"></script>
<script>
    tinyMCE.init({
		mode : "textareas",
		theme: "advanced",
		plugins: "jbimages,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
		language: "en",
		theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4: "jbimages,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location: "top",
		theme_advanced_toolbar_align: "left",
		theme_advanced_statusbar_location: "bottom",
		theme_advanced_resizing: true,
		relative_urls: false
		})
</script>


<form action="insertquestion.php" method="POST">
    <div>
        <h3>Insert Question</h3>
    </div>
<table class="table table-striped" style="margin-top: 35px;">
    <tr>
        <td>Standard</td>
        <td>
            <select id="std" name="std" class="dropdown" style="/*width: 100px;*/" onchange="return GetExam();">
                    <?php
                        $std = GetAllClasses();
                        if(isset($std) && !empty($std)){
                            echo "<option value='Select'>Select Class</option>";
                            while($row = mysql_fetch_assoc($std)){
                                echo "<option value=".$row['Id'].">".$row['Name']."</option>";
                            }
                        }
                    ?>
            </select>
        </td>
    </tr>
    
    <tr>
        <td>Exam</td>
        <td>
            <select id="exam" class="dropdown" name="exam" style="/*width: 200px;*/">        
            </select>
        </td>
    </tr>
    
    <tr>
        <td>Subject</td>
        <td>
            <select id="sub" name="sub" class="dropdown" style="/*width: 200px;*/" onchange="return SubjectChange();">
                    <?php
                        $sub = GetAllSubjects();
                        var_dump($sub);
                        if(isset($sub) && !empty($sub)){
                            echo "<option value='Select'>Select Subject</option>";
                            while($row = mysql_fetch_assoc($sub)){
                                echo "<option value=".$row['Id'].">".$row['Name']."</option>";
                            }
                        }
                    ?>
            </select>
        </td>
    </tr>
    
    <tr>
        <td>Chapter</td>
        <td>
            <select id="chp" name="chp" class="dropdown" style="/*width: 100px;*/">
            </select>
        </td>
    </tr>
    
    <tr>
        <td>Is English</td>
        <td>
            <input type="radio" id="rbtEng" name="lan" value="1">English <br />
            <input type="radio" id="rbtGuj" name="lan" checked="checked" value="0">Gujarati <br />
        </td>
    </tr>
    
    <tr>
        <td>Question</td>
        <td>
            <textarea id="question" name="question"></textarea>
        </td>
    </tr>
    
    <tr>
        <td>Option - 1</td>
        <td>
            <textarea id="opt1" name="opt1"></textarea>
            <input type="checkbox" id="opt-1" value="1" name="opt[]"> <label for="opt-1">IsCorrect</label>
        </td>
    </tr>
    
    <tr>
        <td>Option - 2</td>
        <td>
            <textarea id="opt2" name="opt2"></textarea>
            <input type="checkbox" id="opt-2" value="2" name="opt[]"> <label for="opt-2">IsCorrect</label>
        </td>
    </tr>
    
    <tr>
        <td>Option - 3</td>
        <td>
            <textarea id="opt3" name="opt3"></textarea>
            <input type="checkbox" id="opt-3" value="3" name="opt[]"> <label for="opt-3">IsCorrect</label>
        </td>
    </tr>
    
    <tr>
        <td>Option - 4</td>
        <td>
            <textarea id="opt4" name="opt4"></textarea>
            <input type="checkbox" id="opt-4" value="4" name="opt[]" class="checkbox"> <label for="opt-4">IsCorrect</label>
        </td>
    </tr>
    
    <tr>
        <td>Complexity</td>
        <td>
            <select id="complexity" name="complexity" class="dropdown" style="/*width: 100px;*/">
                    <option value="1">1</option>
                    <option value="2" selected="selected">2</option>
                    <option value="3">3</option>
            </select>
        </td>
    </tr>
    
    <tr>
        <td>Positive Marks</td>
        <td>
            <select id="positivemarks" name="positivemarks" class="dropdown">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
        </td>
    </tr>
    
    <tr>
        <td>Negative Marks</td>
        <td>
            <select id="negativemarks" name="negativemarks" class="dropdown">
                <option value="-0.25">-0.25</option>
                <option value="-1">-1</option>
                <option value="-1.25">-1.25</option>
                <option value="-1.50">-1.50</option>
                <option value="-1.75">-1.75</option>
                
                <option value="-2">-2</option>
                <option value="-2.25">-2.25</option>
                <option value="-2.50">-2.50</option>
                <option value="-2.75">-2.75</option>
                
                <option value="-3">-3</option>
                <option value="-3.25">-3.25</option>
                <option value="-3.50">-3.50</option>
                <option value="-3.75">-3.75</option>
                
                <option value="-4">-4</option>
                <option value="-4.25">-4.25</option>
                <option value="-4.50">-4.50</option>
                <option value="-4.75">-4.75</option>
            </select>
        </td>
    </tr>
    
    <tr>
        <td colspan="2" style="text-align: center;"><input type="submit" name="Submit" value="Submit" /></td>
    </tr>
</table>
</form>

<script>
function SubjectChange(){
    stdId = $("#std").val();
    subId = $("#sub").val();
    
    $.ajax({
            type: "GET",
            url: "../utility/ajaxchapter.php",
            data: "stdId="+stdId+"&subId="+subId+"&type=sel",
            success: function(data){
                $("#chp").empty();
                $("#chp").append("<option value='select'>Select</option>");
                $("#chp").append(data);
            }
    });
}

function GetExam(){
    stdId = $("#std").val();
    $.ajax({
            type:"GET",
            url:"../utility/ajaxgetexam.php",
            data: "stdId="+stdId,
            success: function(data){
                $("#exam").empty();
                $("#exam").append("<option value='select'>Select</option>");
                $("#exam").append(data);
            }
    });
}
</script>

<?php
include_once '../templets/footerTemplate.php';
?>

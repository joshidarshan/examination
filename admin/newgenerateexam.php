<?php
if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'test';
    
//include_once '../templets/adminTemplateHeader.php';
include_once '../templets/adminHeaderNew.php';
include_once '../dbUtility/Class.php';
?>
<form action="subgenerateexam.php" method="POST">
    <div class="contaner">
        <div class="contaner_top-adminuserlisting">
            <div class="subject-adminuserlisting">
                <span style="font-size:40px">Exam Generation</span>
            </div>
        </div>
        <table class="table table-striped" style="margin-top: 35px;">
            <tr style="text-align: left;">
                <td>Exam Name</td>
                <td><input type="text" id="examName" name="examName" /></td>
            </tr>

            <tr style="text-align: left;">
                <td>Standard</td>
                <td>
                    <select id="selStd" name="selStd" onblur="return getSubChp();">
                        <?php
                        $std = GetAllClasses();
                        if (isset($std) && !empty($std)) {
                            echo "<option>Select</option>";
                            while ($row = mysql_fetch_assoc($std)) {
                                echo "<option value=" . $row['Id'] . ">" . $row['Name'] . "</option>";
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>

            <tr style="text-align: left;">
                <td>Subject - Chapter</td>
                <td id="subchp" style="height: 50px; overflow: scroll;">

                </td>
            </tr>

            <tr style="text-align: left;">
                <td>Complexity Level</td>
                <td>
                    <select id="selComplex" name="selComplex">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                </td>
            </tr>

            <tr style="text-align: left;">
                <td>Positive Marks</td>
                <td><input type="text" id="positiveMarks" name="positiveMarks" /></td>
            </tr>

            <tr style="text-align: left;">
                <td>Negative Marks</td>
                <td><input type="text" id="negativeMarks" name="negativeMarks" /></td>
            </tr>

            <tr style="text-align: left;">
                <td>Time for exam</td>
                <td><input type="text" id="time" name="time" /></td>
            </tr>

            <tr style="text-align: left;">
                <td>Valid From</td>
                <td><input type="text" id="validFrom" name="validFrom" class="cal"></td>
            </tr>

            <tr style="text-align: left;">
                <td>Valid To</td>
                <td><input type="text" id="validTo" name="validTo" class="cal"/></td>
            </tr>

            <tr style="text-align: center;">
                <td colspan="2"><input type="submit" name="Submit" value="Submit" /></td>
            </tr>
        </table>
</form>

<script>
                    function getSubChp() {
                        var std = $("#selStd").val();
                        $.ajax({
                            type: "GET",
                            url: "../utility/ajaxdelsubchp.php",
                            data: "std=" + std,
                            success: function(data) {
                                //alert(data);
                                $("#subchp").html(data);
                            }
                        });
                    }
</script>
<?php include_once '../templets/footerTemplate.php'; ?>
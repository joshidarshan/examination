<?php
if(!isset($_SESSION)){ session_start(); }
$_SESSION['cat'] = 'user';
    
if (isset($_POST) && !empty($_POST)) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $role = $_POST['role'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $class = $_POST['Class'];
    $div = $_POST['Division'];
    
    if($class == 'select')
        $class = 0;
    if($div == 'select')
        $div = 0;
 
    include_once '../dbUtility/User.php';
    $id = createUser($username, $password, $role, $firstname, $lastname, $class, $div);
    if(isset($id))
        echo "<script>alert('User created successfully!')</script>";
}
?>

<?php
//include_once '../templets/adminTemplateHeader.php';
include_once '../templets/adminHeaderNew.php';
include_once '../dbUtility/Class.php';
include_once '../dbUtility/division.php';
?>

<form method="POST" onsubmit="return SubmitForm();" action="newuser.php">
    <div>
        <h3>New User</h3>
    </div>
    
    <table class="table table-striped" style="margin-top: 35px;">
        <tr>
            <td>User Name</td>
            <td>
                <input id="username" type="text" name="username" placeholder="UserName" onblur="return getUserAvilability();"/>
                <span id="usernameAvail" style="margin-left: 2px; font-size: large; visibility: hidden;">Available</span>
            </td>
        </tr>
        
        <tr>
            <td>Password</td>
            <td><input type="text" name="password" placeholder="Password" /></td>
        </tr>
        
        <tr>
            <td>Role</td>
            <td>
                <input type="radio" name="role" value="Admin" /><span class="checkbox-spacing">Admin</span>
                <input type="radio" name="role" value="Student" checked="checked" /><span class="checkbox-spacing">Student</span>
            </td>
        </tr>
        
        <tr>
            <td>First Name</td>
            <td><input type="text" name="firstname" placeholder="FirstName" /></td>
        </tr>
        
        <tr>
            <td>Last Name</td>
            <td><input type="text" name="lastname" placeholder="LastName" /></td>
        </tr>
        
        <tr>
            <td>Class</td>
            <td>
                <!--<select id='Class' onchange="return getDivision();" name="Class" class="small-dropdown">-->
                <select id='Class' name="Class" class="small-dropdown">
                        <?php
                        $classResult = GetAllClasses();
                        echo "<option value=''>Select Class</option>";
                        while ($class = mysql_fetch_assoc($classResult)) {
                            echo "<option value=$class[Id]>$class[Name]</option>";
                        }
                        ?>
                </select>
            </td>
        </tr>
        
        <tr>
            <td>Division</td>
            <td>
                <select id='Division' name="Division" class="small-dropdown">
                <?php
                    $divResult = GetAllDivision();
                    echo "<option value='select'>Select Division</option>";
                    while($rdiv = mysql_fetch_assoc($divResult))
                            echo "<option value=$rdiv[Id]>$rdiv[Name]</option>";
                ?>
                </select>
            </td>
        </tr>
        
        <tr>
            <td colspan="2" style="text-align: center;">
                <input type="Submit" value="Submit" name="Submit" class="button btn-large" />
            </td>
        </tr>
    </table>
    
    <script>
        var submt = false;
        function getDivision(){
            classId = document.getElementById("Class").value;
            $.get('../utility/ajaxdivision.php?Id='+classId, function(data){
                var sel = $("#Division");
                sel.empty();
                for(var i=0; i<data.length; i++){
                    //$("#Division").append('<option>'+data[i].Name+'</option>')
                    sel.append('<option value='+data[i].Id+'>'+data[i].Name+'</option>');
                }
            }, "json");
        }
        
        function getUserAvilability(){
            username = $("#username").val();
            $.ajax({
                       type: "GET",
                       url: "../utility/ajaxgetuser.php",
                       data: "Username="+username,
                       success: function(data){
                           if(data > 0){
                               $("#usernameAvail").css("visibility", "visible");
                               $("#usernameAvail").css("color", "red");
                               $("#usernameAvail").html("Not Available");
                               submt = false;
                               return false;
                           }
                           else{
                               $("#usernameAvail").css("visibility", "visible");
                               $("#usernameAvail").css("color", "green");
                               $("#usernameAvail").html("Available");
                               submt = true;
                           }
                       }
                    });
        }
        
        function SubmitForm(){
            if(submt)
                return true;
            else
                return false;
        }
    </script>
</form>    
<?php include_once '../templets/footerTemplate.php'; ?>
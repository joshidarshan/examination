<?php
if (!isset($_SESSION)) {
    session_start();
}
$_SESSION['cat'] = 'question';

// code for postback
if (isset($_FILES) && !empty($_FILES)) {
    include_once './excel_reader2.php';
    include_once '../dbUtility/dbConnect.php';
    Connect();
    // Check for file upload errors
    if ($_FILES["file"]["error"] > 0)
        echo "Error: " . $_FILES["file"]["error"] . "<br/>";
    else {
        move_uploaded_file($_FILES["file"]["tmp_name"], "../upload/" . $_FILES["file"]["name"]);
        $fileLocation = "../upload/" . $_FILES["file"]["name"];

        error_reporting(0);
        $data = new Spreadsheet_Excel_Reader($fileLocation, FALSE);

        // logic for user insertion from excelfile
        for ($i = 2; $i <= $data->rowcount(); $i++) {
            $queSql = "INSERT INTO questions(`question`, `subId`, `chapterId`, `isEnglish`, `complexity`, `createdby`, `PositiveMark`, `NegativeMark`)
                    VALUES('" . $data->val($i, 'A') . "', '" . $data->val($i, 'B') . "', '" . $data->val($i, 'C') . "', '" . $data->val($i, 'D') . "', '" . $data->val($i, 'E') . "','" . $data->val($i, 'F') . "','" . $data->val($i, 'G') . "','" . $data->val($i, 'H') . "')";
            $queQry = mysql_query($queSql);
            $qid = mysql_insert_id();

            $currentOpt = 1;
            for ($j = 9; $j < 13; $j++) {
                $correct = $data->val($i, 13);
                $optSql = "";
                if ($currentOpt == $correct)
                    $optSql = "INSERT INTO `options`(`questionId`, `Option`,`isCorrect`)
                            VALUES('$qid', '" . $data->val($i, $j) . "', '1')";
                else
                    $optSql = "INSERT INTO `options`(`questionId`, `Option`,`isCorrect`)
                            VALUES('$qid', '" . $data->val($i, $j) . "', '0')";
                mysql_query($optSql);
                $currentOpt++;
            }
        }
        unlink($fileLocation);
    }
    Disconnect();
}

include_once '../templets/adminHeaderNew.php';
?>

<form action="questionoptionimport.php" method="POST" enctype="multipart/form-data">
    <div>
        <h3>User Question & Option</h3>
    </div>

    <table class="table table-striped" style="margin-top: 35px;">
        <tr>
            <td>Upload File | </td>
            <td><input type="file" name="file" id="file" /></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Submit" class="button btn-large" /></td>
        </tr>
    </table>

    <?php
    include_once '../templets/footerTemplate.php';
    ?>
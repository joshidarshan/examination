<?php
if (!isset($_SESSION)) {
    session_start();
}
$_SESSION['cat'] = 'report';

include_once '../templets/adminHeaderNew.php';
include_once '../dbUtility/Class.php';

if (empty($_POST) || !isset($_POST)) {
    $classList = GetAllClasses();
    ?>
<form action="report.php" method="post">
    <h3>
        Select Standard to continue...
    </h3>

    <table class="table table-striped">
        <tr>
            <td>Select Class</td>
            <td>
                <select id="class" name="class">
                    <option value="select">Select Class</option>
    <?php
    while ($row = mysql_fetch_assoc($classList))
        echo "<option value='$row[Id]'>$row[Name]</option>";
    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <input type="submit" name="Submit" value="Submit" class="btn btn-large" />
            </td>
        </tr>
    </table>
</form>
    <?php
} else {
    include_once '../dbUtility/report.php';
    include_once '../dbUtility/exam.php';

    $examQuery = GetStdLastExam($_POST['class']);
    $examData = mysql_fetch_assoc($examQuery);
    $lastExamId = $examData['Id'];
    $lastExamId = 25;

    $usrExmData = GetStdLastExamResult($lastExamId);
    $topUsers = GetStdExamTopStudents($lastExamId);
    ?>
    <div style="float: left; width: 80%;">&nbsp;
        <h4>Exam: <?php echo $examData['Name']; ?> Detail</h4>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Action</th>
                <th>Index</th>
                <th>Name</th>
                <th>Score</th>
            </tr>
    <?php
    $countIndex = 1;
    while ($rusrexm = mysql_fetch_assoc($usrExmData)) {
        echo "<tr>";
        echo "<td style='text-align: left; width:150px;'>
                <a href='reportdetail1.php?uid=$rusrexm[userId]' style='margin-right: 10px;'>Select</a>
            </td>";
        echo "<td style='text-align: left; width:150px;'>$countIndex</td>";
        echo "<td style='text-align: left; width:150px;'>$rusrexm[FirstName] &nbsp; $rusrexm[LastName]</td>";
        echo "<td style='text-align: left; width:150px;'>$rusrexm[Mark]</td>";
        echo "</tr>";
        $countIndex++;
    }
    ?>
        </table>
    </div>

<div style="float: right; margin-right: 20px;">&nbsp;
        <h4>Top 10 student</h4>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Index</th>
                <th>Name</th>
                <th>Score</th>
            </tr>
            <?php
            $indCounter = 1;
            while($rtop = mysql_fetch_assoc($topUsers)){
                echo "<tr>";
                echo "<td>$indCounter</td>";
                echo "<td>$rtop[FirstName] &nbsp; $rtop[LastName]</td>";
                echo "<td>$rtop[Mark]</td>";
                echo "</tr>";
                $indCounter++;
            }
            ?>
        </table>
    </div>

    <?php
}
include_once '../templets/adminFooterNew.php';
?>
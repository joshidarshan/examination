<?php
if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'report';
    //include_once '../templets/adminTemplateHeader.php';
    include_once '../templets/adminHeaderNew.php';
    include_once '../dbUtility/report.php';
    $userId = $_GET['uid'];
?>

<!-- design table to be generated
    that contains particular student's all examination link, marks -->

        <?php
            if($userId){
                include_once '../dbUtility/User.php';
                $ud = getUserInfo($userId);
                $userData = mysql_fetch_assoc($ud);
                echo "<h3>Exam Performance Of User: $userData[FirstName] - $userData[LastName]</h3>";
                $data = GetStudentAllExamResult($userId);
                if($data){
                    echo "<table class='table table-striped'>";
                    echo "<tr>";
                    echo "<th>Exam Id</th>";
                    echo "<th>Exam Name</th>";
                    echo "<th>Score</th>";
                    echo "<th>Exam Time</th>";
                    echo "</tr>";

                    foreach($data as $key=>$value){
                        echo "<tr>";
                        echo "<td style='width: 100px;'><a href='reportdetail2.php?uid=".$userId."&examid=".$value['ExamId']."&examName=".$value['Name']."'>".$value['ExamId']."</a></td>";
                        echo "<td style='width: 400px;'><a href='reportdetail2.php?uid=".$userId."&examid=".$value['ExamId']."&examName=".$value['Name']."'>".$value['Name']."</a></td>";
                        echo "<td style='width: 250px;'>".$value['Mark']."</td>";
                        echo "<td style='width: 300px;'>".$value['TimeRange']."</td>";
                        echo "</tr>";
                    }
                    echo "</table>";
                }
                else
                    echo "<strong>No data for the mentioned user</strong>";
            }
            else
                echo "<strong>No user found to show exam details</strong>";
        ?>

<?php
    include_once '../templets/adminFooterNew.php';
?>
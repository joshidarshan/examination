<?php
if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'report';
    
    include_once '../templets/adminHeaderNew.php';
    include_once '../dbUtility/report.php';
    $userId = $_GET['uid'];
    $examId = $_GET['examid'];
    $reviewData = ReviewExam($userId, $examId)
?>

<!-- table: shows particular exam performance details.
     chart: show last 10 exams performance in bar chart.-->
<script src="../RGraph/libraries/RGraph.common.core.js"></script>
<script src="../RGraph/libraries/RGraph.line.js"></script>
<script src="../RGraph/libraries/RGraph.bar.js"></script>

<?php
    if(isset($userId) && !empty($userId) && isset($examId) && !empty($examId))
    {
        $result = GetStudentResultExam($userId, $examId);
        /*
         * $resultData['Marks'] = $mark;
    $resultData['TotalMarks'] = $totalMarks;
    $resultData['Attempt'] = $attemptedQuestion;
    $resultData['UnAttempt'] = $UnAttemptedQuestion;
    $resultData['Correct'] = $correctQuestion;
    $resultData['InCorrect'] = $inCorrectQuestion;
         */
        if(isset($result) && !empty($result)){
            echo "<h3>Performance in exam: $_GET[examName]</h3>";
            echo "<table class='table table-striped table-bordered' style='width: 75%;'>";
           
            echo "<tr><th>Type</th><th>Value</th></tr>";
            
            echo "<tr>";
            echo "<td style='width: 600px;'><strong>Obtained Marks</strong></td>";
            echo "<td>$result[Marks]</td>";
            echo "</tr>";
            
            echo "<tr>";
            echo "<td style='width: 600px;'><strong>Total Marks</strong></td>";
            echo "<td>$result[TotalMarks]</td>";
            echo "</tr>";
            
            echo "<tr>";
            echo "<td style='width: 600px;'><strong>Attempted questions</strong></td>";
            echo "<td>$result[Attempt]</td>";
            echo "</tr>";
            
            echo "<tr>";
            echo "<td style='width: 600px;'><strong>UnAttempted question</strong></td>";
            echo "<td>$result[UnAttempt]</td>";
            echo "</tr>";
            
            echo "<tr>";
            echo "<td style='width: 600px;'><strong>Correct Answers</strong></td>";
            echo "<td>$result[Correct]</td>";
            echo "</tr>";
            
            echo "<tr>";
            echo "<td style='width: 600px;'><strong>InCorrect Answers</strong></td>";
            echo "<td>$result[InCorrect]</td>";
            echo "</tr>";
            
            echo "</table>";
            
            echo"<br/><div style='clear:both; height:15px;'>&nbsp;</div>";
            
            // Area for last exam's subject line chart
            $lineChartData = GetLastExamSectionWiseScore($userId, $examId);
            $labels = array();
            $data = array();
            
            foreach($lineChartData as $key=>$val){
                $labels[] = $val['Name'];
                $data[] = $val['Mark'];
            }
            
            $data_string = "[" . join(", ", $data) . "]";
            $labels_string = "['" . join("', '", $labels) ."']";
            
            $lastTenExamChart = GetStudentLastExamsResult($userId);
            $leLabels = array();
            $leData = array();
            
            while($row = mysql_fetch_assoc($lastTenExamChart)){
                $leLabels[] = $row['Name'];
                $leData[] = $row['Mark'];
            }
            
            $le_data_string = "[" . join(", ", $leData) . "]";
            $le_labels_string = "['" . join("', '", $leLabels) ."']";
            
            $tsTimeExamChart = GetStudentExamPerformanceByTime($userId, $examId);
            $tsLabels = array();
            $tsData = array();
            
            while($row = mysql_fetch_assoc($tsTimeExamChart)){
                $tsLabels[] = $row['Name'];
                $tsData[] = $row['Time'];
            }
            
            $te_data_string = "[" . join(", ", $tsData) ."]";
            $te_labels_string = "['" . join("', '", $tsLabels) ."']";
        }
        else
            echo "No enough information available of the student";
    }
    else
        echo "No Data Found";
?>
<!--<h2>Section wise score for last exam:</h2>
 <canvas id='cvs' width='600' height='250'>[Kindly upgrade to modern browser. Your browser does not support this feature.]</canvas>
<script>
    chart = new RGraph.Bar('cvs', <?php //print($data_string) ?>);
    chart.Set('chart.background.grid.autofit', true);
    chart.Set('chart.gutter.left', 35);
    chart.Set('chart.gutter.right', 5);
    chart.Set('chart.hmargin', 10);
    chart.Set('chart.tickmarks', 'endcircle');
    chart.Set('chart.labels', <?php //print($labels_string) ?>);        
    chart.Draw();
</script>

<br /><br />-->
<div>
<h3>Last ten exam performance:</h3>
<canvas id="cvsLE" width="600" height="250">[Kindly upgrade to modern browser. Your browser does not support this feature.]</canvas>
<script>
    lchart = new RGraph.Bar('cvsLE', <?php print($le_data_string) ?>);
    lchart.Set('chart.background.grid.autofit', true);
    lchart.Set('chart.gutter.left', 35);
    lchart.Set('chart.gutter.right', 5);
    lchart.Set('chart.hmargin', 10);
    lchart.Set('chart.tickmarks', 'endcircle');
    lchart.Set('chart.labels', <?php print($le_labels_string) ?>);
    lchart.Draw();
</script>
</div>

<!--<h2>Time spend over subjects in last exam:</h2>
<canvas id="cvsTS" width="800" height="250">[Kindly upgrade to modern browser. Your browser does not support this feature.]</canvas>
<script>
    tchart = new RGraph.Line('cvsTS', <?php //print($te_data_string) ?>);
    tchart.Set('chart.background.grid.autofit', true);
    tchart.Set('chart.gutter.left', 35);
    tchart.Set('chart.gutter.right', 5);
    tchart.Set('chart.hmargin', 10);
    tchart.Set('chart.tickmarks', 'endcircle');
    tchart.Set('chart.labels', <?php //print($te_labels_string) ?>);
    tchart.Draw();
</script>-->
<!--<canvas id='line1' width='600' height='250'>[No canvas support]</canvas>
<script>
    var line1 = new RGraph.Line('line1', <?php //print($data_string) ?>)
    line1.Set('chart.background.grid.autofit', true);
    line1.Set('chart.gutter.left', 35);
    line1.Set('chart.gutter.right', 5);
    line1.Set('chart.hmargin', 10);
    line1.Set('chart.tickmarks', 'endcircle');
    line1.Set('chart.labels', <?php //print($labels_string) ?>);
    line1.Draw();
</script>-->

<!-- Showing exam review of particular user -->
<?php
echo "<h3>Exam review: </h3>";
echo "<div style='clear: both; float: left; margin-top: 5px; text-align: left;'>&nbsp;";
    echo "<table class='table table-striped table-bordered'>";
foreach ($reviewData as $key=>$value){
    if($value['time'] > 0){
    
    echo "<tr>";
    echo "<td>Question: </td>";
    echo "<td>$value[question]</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>User Answer: </td>";
    echo "<td>$value[answer]</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Correct Answer(s): </td>";
    echo "<td>$value[correctAns]</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Is Correct: </td>";
    echo "<td>$value[correct]</td>";
    echo "</tr>";
    
    
    echo "<tr>";
    echo "<td>Is Question Attempted</td>";
    echo "<td>$value[attampt]</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Number of time question visited</td>";
    echo "<td>$value[visit]</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Time taken: [In Seconds]</td>";
    echo "<td>$value[time]</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Marks: </td>";
    if($value['mark'] < 0)
        echo "<td style='color: red;'>$value[mark]</td>";
    else if($value['mark'] > 0)
        echo "<td style='color: green;'>$value[mark]</td>";
    else 
        echo "<td>$value[mark]</td>";
    echo "</tr>";
    echo "<tr><td colspan='2'><hr /></td></tr>";
    }
}
echo "</table>";
    echo "</div>";
?>
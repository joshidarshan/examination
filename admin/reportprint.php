<?php
if (!isset($_SESSION)) {
    session_start();
}
$_SESSION['cat'] = 'report';

include_once '../templets/adminHeaderNew.php';
include_once '../dbUtility/Class.php';
$userList = GetAllClasses();
?>

<?php
if (empty($_POST) || !isset($_POST)) {
    ?>
    <h3>Report Printing</h3>
    <form action="reportprint.php" method="POST">
    <table class="table table-hover table-striped table-bordered">
        <tr>
            <td>Select Standard</td>
            <td>
                <select id="std" name="std" onblur="return getStudent();">
                    <option value="select">Select</option>
                    <?php
                    while ($urow = mysql_fetch_assoc($userList)) {
                        echo "<option value='$urow[Id]'>$urow[Name]</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>

        <tr>
            <td>Select Student</td>
            <td>
                <select id="student" name="student">
                    <?php ?>
                </select>
            </td>
        </tr>
        
        <tr>
            <td colspan='2' style='text-align: center;'>
                <input type='submit' name='submit' value='Submit' class='btn btn-large' />
            </td>
        </tr>
    </table>
</form>
    <?php
} else {
    ?>
<h3>Select exam to print report</h3>
<form action="reportprint1.php" method="POST">
    <?php
    include_once '../dbUtility/report.php';
    $userExamData = GetStudentAllExamResult($_POST['student']);
    
    echo "<input type='hidden' id='user' name='user' value='$_POST[student]' />";
    echo "<table class='table table-striped table-bordered'>";
    echo "<tr><th>Action</th><th>Exam</th><th>Score</th></tr>";
    foreach($userExamData as $uekey=>$uevalue){
        echo "<br />";
        //SELECT ex.Id as examId, ex.startFrom, ex.endTo, ex.Name, sum(ue.mark) as Mark
?>

    <tr>
        <td><input type="checkbox" name="exmId[]" value="<?php echo $userExamData[$uekey]['ExamId'];?>" /></td>
        <td><?php echo $userExamData[$uekey]['Name']; ?></td>
        <td><?php echo $userExamData[$uekey]['Mark']; ?></td>
    </tr>

<?php
    }
    echo "<tr><td colspan='3' style='text-align: center;'><input type='submit' name='submit' value='Submit' class='btn btn-large' /></td></tr>";
    echo "</table>";
    
}
?>
</form>
<script>
                function getStudent() {
                    var std = $("#std").val();

                    $.ajax({
                        type: "GET",
                        url: "../utility/ajaxreport.php",
                        data: "type=users&class=" + std + "&div=1",
                        success: function(data) {
                            $("#student").empty();
                            $("#student").append("<option value='select'>Select</option>");
                            $("#student").append(data);
                        }
                    });
                }
</script>

<?php
include_once '../templets/footerTemplate.php';
?>
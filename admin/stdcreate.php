<?php 
    if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'class';
    
    include_once '../dbUtility/Class.php';
    
    if(isset($_POST) && !empty($_POST)){
        $std = $_POST['name'];
        $stdId = CreateClass($std);
        echo "<script>alert('Standard: $std Created')</script>";
    }
    
    include_once '../templets/adminHeaderNew.php';
?>

<form action="stdcreate.php" method="POST">
    <div class="contaner">
        <div class="contaner_top-adminuserlisting">
            <div class="subject-adminuserlisting">
                <h3>New Standard</h3>
            </div>
        </div>
        <!-- style="width: 90%; margin-left: 50px;" -->
        <table class="table table-striped">
            <tr>
                <td>Name</td>
                <td><input type="text" name="name" id="name" /></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <input type="submit" name="submit" value="Submit" class="btn btn-large" />
                </td>
            </tr>
        </table>
        
    </div>
</form>
<?php include_once '../templets/footerTemplate.php'; ?>
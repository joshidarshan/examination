<?php
if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'class';

include_once '../dbUtility/Class.php';
$classQuery = GetClass($_GET['cid']);
$classData = mysql_fetch_assoc($classQuery);

// Handling post back
if(!empty($_POST) && isset($_POST)){
    $classId = $_POST['cid'];
    $className = $_POST['class'];
    $status = $_POST['rbtStatus'];
    
    $classUpdate = SetClass($classId, $className, $status);
    header('location: stdlisting.php');
}
include_once '../templets/adminHeaderNew.php';
?>

<form action="stdedit.php" method="POST">
    <div>
        <h3>Edit Class</h3>
    </div>
<table class="table table-striped" style="margin-top: 35px;">
    <tr>
        <td>Class</td>
        <td>
            <input type="text" id="class" name="class" value=<?php echo $classData['Name']; ?> />
        </td>
    </tr>
    <tr>
        <td>Status: </td>
        <td>
            <input type="radio" class="radio" name="rbtStatus" <?php if($classData['Status'] == '1') echo "checked=checked" ?> value="1">Active <br />
            <input type="radio" class="radio" name="rbtStatus" <?php if($classData['Status'] == '0') echo "checked=checked" ?> value="0">De-active
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Submit" class="btn btn-large" /></td>
    </tr>
</table>
    <input type="hidden" id="id" name="cid" value="<?php echo $_GET['cid']; ?>" />
</form>

<?php
include_once '../templets/footerTemplate.php';
?>
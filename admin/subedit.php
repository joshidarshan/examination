<?php
if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'subject';

include_once '../dbUtility/Subject.php';

$subjectQuery = GetAllSubjects();
$subjectData = mysql_fetch_assoc($subjectQuery);

// Handling post back
if(!empty($_POST) && isset($_POST)){
    $subject = $_POST['subject'];
    $subId = $_POST['sid'];
    $status = $_POST['rbtStatus'];
    
    SetSubject($subId, $subject, $status);
    header('location: sublisting.php');
}
include_once '../templets/adminHeaderNew.php';
?>

<form action="subedit.php" method="POST">
    <div>
        <h3>Edit Subject</h3>
    </div>
<table class="table table-striped" style="margin-top: 35px;">
    <tr>
        <td>Subject</td>
        <td><input type="text" id="subject" name="subject" value="<?php echo $subjectData['Name']; ?>" /></td>
    </tr>
    <tr>
        <td>Status: </td>
        <td>
            <input type="radio" class="radio" name="rbtStatus" <?php if($subjectData['Status'] == '1') echo "checked=checked" ?> value="1">Active <br />
            <input type="radio" class="radio" name="rbtStatus" <?php if($subjectData['Status'] == '0') echo "checked=checked" ?> value="0">De-active
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Submit" class="btn btn-large" /></td>
    </tr>
</table>
    <input type="hidden" id="sid" name="sid" value="<?php echo $_GET['sid']; ?>" />
</form>

<?php
include_once '../templets/footerTemplate.php';
?>
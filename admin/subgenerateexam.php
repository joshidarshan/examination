<?php

include_once '../dbUtility/Subject.php';
include_once '../dbUtility/Chapter.php';
$qidList = array();
$finalQuestionList = array();

// storing standardid
$stdId = $_POST['selStd'];

// Complexity level
$complexity = $_POST['selComplex'];
$easy = $tough = $medium = 0;

// Getting qid list for last exam
$lastExamId = GetLastExamId($stdId);

// Getting qid string for last exam
$lastExamQid = GetExamQidString($lastExamId);

// Getting chapter list for given stdId and filling with 0.
$chapter = GetStandardChapters($stdId);
$chapterList = array();
while ($row = mysql_fetch_assoc($chapter)) {
    $chapterList[$row['Id']] = 0;
}

// Accessing POST data for chapters que count. Updating chapterList.
foreach ($chapterList as $pkey => $pvalue) {
    if (!empty($_POST[$pkey]) && isset($_POST[$pkey])) {
        $chapterList[$pkey] = $_POST[$pkey];
    }
}

// Iterating and checking chapterList for non-zero item
// And fetching unique questionIds.
$remain = 0;
foreach ($chapterList as $key => $value) {
    $queCounter = 0;
    $qidString = "";

    if ($value != 0) {
        if ($complexity == '1') {
            $easy = round((intval($value) * 50) / 100);
            $medium = round((intval($value) * 30) / 100);
            $tough = round((intval($value) * 20) / 100);
            
            $emtc = $easy+$medium+$tough;
            if(intval($value) < $emtc)
                $easy = $easy-($emtc - intval($value));
        } else if ($complexity == '2') {
            $medium = round((intval($value) * 50) / 100);
            $easy = round((intval($value) * 30) / 100);
            $tough = round((intval($value) * 20) / 100);
            
            $emtc = $easy+$medium+$tough;
            if(intval($value) < $emtc)
                $medium = $medium($emtc - intval($value));
        } else if ($complexity == '3') {
            $tough = round((intval($value) * 50) / 100);
            $medium = round((intval($value) * 30) / 100);
            $easy = round((intval($value) * 20) / 100);
            
            $emtc = $easy+$medium+$tough;
            if(intval($value) < $emtc)
                $tough = $tough-($emtc - intval($value));
        }

        // Filling question according to complexity level.
        for ($i = 1; $i < 4; $i++) {
            $chpQid = GetChapterQuestions($key, $i);
            shuffle($chpQid);
            $result = array_diff($chpQid, $lastExamQid);

            // need to specify which toughness level
            // If i=1 then use easy as question count for no of easy questions
            // Add questions to qid list
            if ($i == 1) {
                $easyQuestions = "";
                if (count($result) < $easy) {
                    $easyQuestions = array_slice($result, 0, count($result));
                    $remain += count($result) - $easy;
                }
                else
                    $easyQuestions = array_slice($result, 0, $easy);

                // entering easy question into qidlist
                foreach ($easyQuestions as $ekey => $evalue)
                    array_push($qidList, $evalue);
            }
            else if ($i == 2) {
                $midQuestions = "";
                if (count($result) < $medium) {
                    $midQuestions = array_slice($result, 0, count($result));
                    $remain += count($result) - $medium;
                }
                else
                    $midQuestions = array_slice($result, 0, $medium);

                // entering medium question into qidlist
                foreach ($midQuestions as $mkey => $mvalue)
                    array_push($qidList, $mvalue);
            }
            else if ($i == 3) {
                $toughQuestions = "";
                if (count($result) < $tough) {
                    $toughQuestions = array_slice($result, 0, count($result));
                    $remain += count($result) - $tough;
                }
                else
                    $toughQuestions = array_slice($result, 0, $tough);

                // entering tough question into qidlist
                foreach ($toughQuestions as $tkey => $tvalue)
                    array_push($qidList, $tvalue);
            }

            // If remain count is grater than 0 find chapter question and add until remain count is 0
            $queToAdd = GetNumChapterQuestion($key, $remain, $qidList);
            // Adding remained question into qidlist
            foreach ($queToAdd as $rkey => $rvalue)
                array_push($qidList, $rvalue);
        }
    }
}

print_r($qidList);
?>

<?php

function GetLastExamId($stdId) {
    Connect();
    $lastExamIdSql = "SELECT Id
                      FROM exams
                      WHERE `classId` = '$stdId' AND `status` = '1'
                      ORDER BY Id DESC
                      LIMIT 0, 1";
    $lastExamIdQuery = mysql_query($lastExamIdSql);
    $lastExamIdData = mysql_fetch_assoc($lastExamIdQuery);
    Disconnect();
    return $lastExamIdData['Id'];
}

function GetExamQidString($examId) {
    Connect();
    $lastExamQidSql = "SELECT `questionId`
                            FROM examquestions
                            WHERE `examId` = '$examId'";
    $lastExamQidQuery = mysql_query($lastExamQidSql);
    $lastExamData = array();
    $lastExamQidString = "";
    while ($row = mysql_fetch_assoc($lastExamQidQuery)) {
        $lastExamData[] = $row['questionId'];
    }
    /* for ($i = 0; $i < count($lastExamData); $i++) {
      if ($i == (count($lastExamData) - 1))
      $lastExamQidString .= $lastExamData[$i];
      else
      $lastExamQidString .= $lastExamData[$i] . ",";
      }
      Disconnect();
      return $lastExamQidString; */
    Disconnect();
    return $lastExamData;
}

function GetChapterQuestions($chpId, $complexity) {
    Connect();
    $chapterQuestionSql = "SELECT `Id`
                            FROM questions
                            WHERE `chapterId` = '$chpId' AND `complexity` = '$complexity' AND status = '1'";
    $chapterQuestionQuery = mysql_query($chapterQuestionSql);
    $chapterQidList = array();
    while ($row = mysql_fetch_assoc($chapterQuestionQuery)) {
        $chapterQidList[] = $row['Id'];
    }
    Disconnect();
    return $chapterQidList;
}

function GetNumChapterQuestion($chpId, $count, $previousQid) {
    if (is_array($previousQid))
        $previousQid = GetArrayToCsv($previousQid);
    Connect();
    $sql = "SELECT id
            FROM questions
            WHERE `chapterId` = '$chpId' AND `Id` NOT IN ($previousQid)
            ORDER BY RAND()
            LIMIT 0, $count";
    $query = mysql_query($sql);
    Disconnect();
    $qid = array();
    while ($row = mysql_fetch_assoc($query))
        $qid[] = $row['id'];
    return $qid;
}

function GetArrayToCsv($arr) {
    $csv = "";
    for ($i = 0; $i < count($arr); $i++) {
        if ($i == (count($arr) - 1))
            $csv .= $arr[$i];
        else
            $csv .= $arr[$i] . ",";
    }
    return $csv;
}
?>
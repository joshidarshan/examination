<?php 
    if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'subject';
    
    include_once '../dbUtility/Subject.php';
        
    if(isset($_POST) && !empty($_POST)){
        $subjectName = $_POST['subject'];
        $subId = AddSubject($subjectName);
        /*if(isset($divId) && !empty($divId))
            echo "<script>alert('Division: $divId Created')</script>";*/
    }
    
    include_once '../templets/adminHeaderNew.php';
?>

<form action="subjectcreate.php" method="POST">
    <div class="contaner">
        <div class="contaner_top-adminuserlisting">
            <div class="subject-adminuserlisting">
                <h3>New Subject</h3>
            </div>
        </div>
        <!-- style="width: 90%; margin-left: 50px;" -->
        <table class="table table-striped">
            <tr>
                <td>Subject</td>
                <td><input type="text" class="text" name="subject" id="subject" /></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <input type="submit" name="submit" value="Submit" class="btn btn-large" />
                </td>
            </tr>
        </table>
        
    </div>
</form>
<?php include_once '../templets/footerTemplate.php'; ?>
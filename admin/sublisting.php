<?php 
    if(!isset($_SESSION)){ session_start(); }
    $_SESSION['cat'] = 'subject';
    
    include_once '../dbUtility/Subject.php';
    $subjects = GetAllSubjects();
    
    include_once '../templets/adminHeaderNew.php';
?>

<form action="userlisting.php" method="POST">
    <div class="contaner">
        <div class="contaner_top-adminuserlisting">
            <div class="subject-adminuserlisting">
                <h3>Class Listing</h3>
            </div>
        </div>
        <!-- style="width: 90%; margin-left: 50px;" -->
        <table class="table table-striped">
            <tr style='text-align: left;'>
                <th>Action</th>
                <th>Id</th>
                <th>Name</th>
                <th>Status</th>
            </tr>
            <?php
                while($row = mysql_fetch_assoc($subjects)){
                    echo "<tr>";
                    echo "<td style='text-align: left; width:150px;'>
                            <a href='subedit.php?sid=$row[Id]' style='margin-right: 10px;'>Edit</a>
                            <a href='subdetail.php?sid=$row[Id]' style='margin-right: 10px;'>Select</a>
                            <a href='subdelete.php?sid=$row[Id]' style='margin-right: 10px;'>Delete</a>
                        </td>";
                    echo "<td style='text-align: left; width:150px;'>$row[Id]</td>";
                    echo "<td style='text-align: left; width:150px;'>$row[Name]</td>";
                    
                    if($row['Status'] == 1)
                        echo "<td style='text-align: left; width:150px;'>Active</td>";
                    else
                        echo "<td style='text-align: left; width:150px;'>Deactive</td>";
                    
                    echo "</tr>";
                }
            ?>
        </table>
        
    </div>
</form>
<?php include_once '../templets/footerTemplate.php'; ?>
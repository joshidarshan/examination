<?php
if (!isset($_SESSION)) {
    session_start();
}
$_SESSION['cat'] = 'user';

include_once '../templets/adminHeaderNew.php';
include_once '../dbUtility/Class.php';

if (empty($_POST) || !isset($_POST)) {
    $classList = GetAllClasses();
    ?>
<form action="userlisting.php" method="post">
    <h3>
        Select Standard to continue...
    </h3>

    <table class="table table-striped">
        <tr>
            <td>Select Class</td>
            <td>
                <select id="class" name="class">
                    <option value="select">Select Class</option>
    <?php
    while ($row = mysql_fetch_assoc($classList))
        echo "<option value='$row[Id]'>$row[Name]</option>";
    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <input type="submit" name="Submit" value="Submit" class="btn btn-large" />
            </td>
        </tr>
    </table>
</form>
    <?php
} else {
    include_once '../dbUtility/User.php';
    $users = GetStdUsers($_POST['class']);
//SELECT us.`Id`, us.`UserName`, us.`Role`, us.`FirstName`, us.`LastName`, us.`ClassId`, us.`DivId`, cl.`Name` as class, dv.`Name` as division
    echo "<table class='table table-striped'>";
    echo "<tr><th>Action</th><th>Index</th><th>Name</th><th>Std - Div";
    $rcount = 1;
    while($ruser = mysql_fetch_assoc($users)){
        echo "<tr>";
        echo "<td style='text-align: left; width:150px;'>
                <a href='edituser.php?uid=$ruser[Id]' style='margin-right: 10px;'>Edit</a>
                <a href='detailuser.php?uid=$ruser[Id]' style='margin-right: 10px;'>Select</a>
                <a href='deleteuser.php?uid=$ruser[Id]' style='margin-right: 10px;'>Delete</a>
            </td>";
        echo "<td style='text-align: left; width:150px;'>$rcount</td>";
        echo "<td style='text-align: left; width:150px;'>$ruser[FirstName]"."  "."$ruser[LastName]</td>";
        echo "<td style='text-align: left; width:150px;'>$ruser[class]"." - "."$ruser[division]</td>";
        echo "</tr>";
        
        $rcount++;
    }
    echo "</table>";
}
include_once '../templets/adminFooterNew.php';
?>
<?php
session_start();
$pageName;
if (!isset($pageName))
    $pageName = "Home";

$login = false;
if (isset($_SESSION['UID']) && !empty($_SESSION['UID']))
    $login = TRUE;
else
    header("Location: login.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../style/style.css" type="text/css" />
        <link rel="stylesheet" href="../style/menu.css" type="text/css" />
        <script type="text/javascript" src="../js/jquery-1.9.1.js"></script>
        <title>Core education | <?php echo $pageName; ?></title>
    </head>
    <body>
        <div class="header1">
            <div class="header1_top">
                <div class="header_top_logo">
                    <img src="../image/logo.png" class="img_logo" />
                </div>
                <div class="header_top_note">
                    <?php
                    if ($login) {
                        echo "<h2>
                            Welcome " . $_SESSION['Name'] . "<br />
                            <a href='../logout.php' class='link-nostyle'>LogOut</a>
                          </h2>";
                    } else {
                        echo "<h2>
                            Welcome<br />
                            <a href='login.php'  class='link-nostyle'>Click to Login</a>
                          </h2>";
                    }
                    ?>
                </div>
            </div>
            <div class="header1_nav">
                <ul class="mn-container">
                    <li><a href="../index.php">Home</a></li>
                    <!--<li><a href="attendance.php">Attendance</a></li>-->
                    <li style="margin-left: 30px;"><a href="../examListing.php">Test</a></li>
                    <li style="margin-left: 40px;"><a href="../creport/report.php">Report</a></li>
                    <!--<li><a href="message.php">Message</a></li>-->
                    <li style="margin-left: 40px;"><a href="../creport/review.php">Review</a></li>
                    <li style="margin-left: 50px;"><a href="#">Users</a></li>
                </ul>
            </div>
        </div>
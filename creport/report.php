<?php
    include_once 'clienttemplate.php';
    include_once '../dbUtility/report.php';
    $userId = $_SESSION['UID'];
?>

<!-- design table to be generated
    that contains particular student's all examination link, marks -->

<div class="contaner">
    <div class="contaner_top">
        
        <?php
            if($userId){
                $data = GetStudentAllExamResult($userId);
                if($data){
                    echo "<table width='100%'>";
                    echo "<tr>";
                    echo "<th>Exam Id</th>";
                    echo "<th>Exam Name</th>";
                    echo "<th>Score</th>";
                    echo "<th>Exam Time</th>";
                    echo "</tr>";

                    foreach($data as $key=>$value){
                        echo "<tr>";
                        echo "<td style='width: 100px;'><a href='reportdetail1.php?uid=".$userId."&examid=".$value['ExamId']."'>".$value['ExamId']."</a></td>";
                        echo "<td style='width: 400px;'><a href='reportdetail1.php?uid=".$userId."&examid=".$value['ExamId']."'>".$value['Name']."</a></td>";
                        echo "<td style='width: 250px;'>".$value['Mark']."</td>";
                        echo "<td style='width: 300px;'>".$value['TimeRange']."</td>";
                        echo "</tr>";
                    }
                    echo "</table>";
                }
                else
                    echo "<strong>No data for the mentioned user</strong>";
            }
            else
                echo "<strong>No user found to show exam details</strong>";
        ?>
      
    </div>
</div>

<?php
    include_once '../templets/footerTemplate.php';
?>
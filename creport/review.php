<?php
include_once './clienttemplate.php';
include_once '../dbUtility/exam.php';
include_once '../dbUtility/report.php';
$exm = StudentLastExam($_SESSION['UID']);
$reviewData = ReviewExam($_SESSION['UID'], $exm);
?>

<?php
foreach ($reviewData as $key=>$value){
    /*
     *  $local_array['qid'] = $row['qid'];
        $local_array['question'] = $row['question'];
        $local_array['correctAns'] = $row['opt'];
        $local_array['answer'] = '0';
        $local_array['correct'] = '0';
        $local_array['attampt'] = '0';
        $local_array['visit'] = '0';
        $local_array['time'] = '0';
     */
    echo "<div style='clear: both; float: left; margin-top: 5px; text-align: left;'>&nbsp;";
    echo "<table border='1'>";
    echo "<tr>";
    echo "<td>Question: </td>";
    echo "<td>$value[question]</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>User Answer: </td>";
    echo "<td>$value[answer]</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Correct Answer(s): </td>";
    echo "<td>$value[correctAns]</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Is Correct: </td>";
    echo "<td>$value[correct]</td>";
    echo "</tr>";
    
    
    echo "<tr>";
    echo "<td>Is Question Attempted</td>";
    if(isset($value['answer']) && !empty($value['answer']) && $value['answer'] != 0)
        echo "<td>$value[attampt]</td>";
    else
        echo "<td>0</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Number of time question visited</td>";
    echo "<td>$value[visit]</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Time taken: [In Seconds]</td>";
    echo "<td>$value[time]</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Marks: </td>";
    echo "<td>$value[mark]</td>";
    echo "</tr>";
    
    echo "</table>";
    echo "</div>";
}
?>

<?php
include_once './footerTemplate.php';
?>
<?php
include_once 'dbConnect.php';

function CreateChapter($name, $stdId, $subId){
    Connect();
    $sql = "INSERT INTO chapters(`Name`, `stdId`, `subId`, `Status`)
            VALUES('$name', '$stdId', '$subId', '1')";
    $query = mysql_query($sql);
    $affectedrow = mysql_affected_rows();
    Disconnect();
    return $affectedrow;
}

function DeleteChapter($chpId){
    Connect();
    $sql = "UPDATE chapters
            SET `Status` = '0'
            WHERE `Id` = '$chpId'";
    $query = mysql_query($sql);
    $affectedrow = mysql_affected_rows();
    Disconnect();
    return $affectedrow;
}

function SetChapter($chpId, $name, $stdId, $subId, $status){
    Connect();
    $sql = "UPDATE chapters
            SET `Name` = '$name', `stdId` = '$stdId', `subId` = '$subId', `Status` = '$status'
            WHERE `Id` = '$chpId'";
    $query = mysql_query($sql);
    $affectedrow = mysql_affected_rows();
    Disconnect();
    return $affectedrow;
}

function GetChapter($chpId){
    Connect();
    $sql = "SELECT `Id`, `Name`, `stdId`, `subId`, `Status`
            FROM chapters
            WHERE `Id` = '$chpId'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetSubjectChapter($stdId, $subId){
    Connect();
    $sql = "SELECT ch.`Id`, ch.`Name`
            FROM chapters ch
            JOIN classes st
                ON ch.`stdId` = st.`Id`
            WHERE ch.`subId` = '$subId' AND ch.`stdId` = '$stdId'";
    $sqlQuery = mysql_query($sql);
    Disconnect();
    return $sqlQuery;
}

function GetStandardChapters($stdId){
    Connect();
    $sql = "SELECT `Id`, `Name`, `subId`
            FROM chapters
            WHERE `stdId` = '$stdId'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetChapterListing(){
    Connect();
    $sql = "SELECT ch.`Id` as chid, ch.`Name` as chname, sb.`Id` as sbid, sb.`Name` as sbname, cl.`Id` as clid, cl.`Name` as clname, ch.`Status`
            FROM chapters ch
            JOIN subjects sb
                ON ch.`subId` = sb.`Id`
            JOIN classes cl
                ON ch.`stdId` = cl.`Id`
            ORDER BY cl.`Id`";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
    
}
?>

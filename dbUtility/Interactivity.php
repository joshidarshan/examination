<?php    
    include_once 'dbConnect.php';
    include_once '../Utility.php';
    
    function AddInteractivity($userId, $subjectId, $iLevel){
        Connect();
        $time = GetCurrentTime();
        $sql = "INSERT INTO interctivities(`UserId`, `Level`, `Date`, `LectureSubject`, `Status`)
                VALUES('$userId', '$iLevel', '$time', '$subjectId', '1')";
        $result = mysql_query($sql);
        $affectedrow = mysql_affected_rows();
        Disconnect();
        return $affectedrow;
    }
    
    function DeleteInteractivity($Id){
        Connect();
        $sql = "UPDATE interctivities 
                SET `Status` = '0' 
                WHERE `Id` = '$Id'";
        $result = mysql_query($sql);
        $affectedrow = mysql_affected_rows();
        Disconnect();
        return $affectedrow;
    }
    
    function SetInteractivity($id, $userId, $subjectId, $iLevel, $time){
        Connect();
        $sql = "UPDATE interctivities
                SET `UserId` = '$userId', `Level` = '$iLevel', `Date` = '$time', `LectureSubject` = '$subjectId', `Status` = '1'
                WHERE `Id`= '$id'";
        $result = mysql_query($sql);
        $affectedrow = mysql_affected_rows();
        Disconnect();
        return $affectedrow;
    }
    
    function GetAllInteractivityForClass($classId){
        Connect();
        $sql = "SELECT it.`Id`, it.`UserId`, it.`Level`, it.`Date`, it.`LectureSubject`, cl.`Name`, su.`Name`
                FROM interctivities it
                JOIN users us
                    ON it.`UserId` = us.`Id`
                JOIN classes cl
                    ON us.`ClassId` = cl.`Id`
                JOIN subjects su
                    ON it.`LectureSubject` = su.`Id`
                WHERE cl.`Id` = '$classId' AND it.`Status` = '1'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function GetAllInteractivityByClass(){
        Connect();
        $sql = "SELECT it.`Id`, it.`UserId`, it.`Level`, it.`Date`, it.`LectureSubject`, it.`Status`, cl.`Name`, su.`Name`
                FROM interctivities it
                JOIN users us
                    ON it.`UserId` = us.`Id`
                JOIN classes cl
                    ON us.`ClassId` = cl.`Id`
                JOIN subjects su
                    ON it.`LectureSubject` = su.`Id`
                GROUP BY cl.`Id`
                HAVING it.`Status` = '1'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function GetInteractivityForStudentTimeRange($userId, $startDate, $endDate){
        Connect();
        $sql = "SELECT it.`Id`, it.`UserId`, it.`Level`, it.`Date`, it.`LectureSubject`, it.`Status`, cl.`Name`, su.`Name`
                FROM interctivities it
                JOIN users us
                    ON it.`UserId` = us.`Id`
                JOIN classes cl
                    ON us.`ClassId` = cl.`Id`
                JOIN subjects su
                    ON it.`LectureSubject` = su.`Id`
                WHERE it.`UserId` = '$userId' AND it.`Date` >= '$startDate' AND it.`Date` <= '$endDate'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    
?>

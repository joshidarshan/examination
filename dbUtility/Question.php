<?php
    include_once 'dbConnect.php';
    
    // function for inserting question
    function InsertQuestion($question, $subId, $chpId, $complex, $language, $positiveMarks, $negativeMarks){
        Connect();
        $queInsertSql = "INSERT INTO questions(`question`, `subId`, `chapterId`, `isEnglish`, `complexity`,`PositiveMark`, `NegativeMark`, `status`)
                         VALUES('$question', '$subId', '$chpId', '$language', '$complex', '$positiveMarks', '$negativeMarks', '1')";
        $queInsertData = mysql_query($queInsertSql);
        $qid = mysql_insert_id();
        Disconnect();
        return $qid;
    }
    
    // function for editing question
    function EditQuestion($qid, $question, $chpId, $complex){
        Connect();
        $queEditSql = "UPDATE questions
                        SET `question` = '$question', `chapterId` = '$chpId', `complexity` = '$complex'
                        WHERE `Id` = '$qid'";
        $queEditData = mysql_query($queEditSql);
        $queEditAffectedRows = 0;
        if($queEditData)
            $queEditAffectedRows = mysql_affected_rows ();
        Disconnect();
        return $queEditAffectedRows;
    }
    
    // function for deleting question
    function DeleteQuestion($qid){
        Connect();
        $queDeleteSql = "UPDATE questions
                         SET `status` = '0'
                         WHERE `Id` = '$qid'";
        $queDeleteData = mysql_query($queDeleteSql);
        $queDeleteAffectedRows = 0;
        if($queDeleteData)
            $queDeleteAffectedRows = mysql_affected_rows ();
        Disconnect();
        return $queDeleteAffectedRows;
    }
    
    // function for getting question
    function GetQuestion($qid, $ajax=false){
        Connect();
        $queSelectSql = "SELECT question
                FROM questions
                WHERE `Id` = '$qid'";
        $queSelectData = mysql_query($queSelectSql);
        $questionData = mysql_fetch_row($queSelectData);
        if($ajax)
            $question = json_encode($questionData[0]);
        else
            $question = $questionData[0];
        Disconnect();
        return $question;
    }
    
    function GetQuestionMark($qid){
        Connect();
        $sql = "SELECT `PositiveMark`, `NegativeMark`
                FROM questions
                WHERE `Id` = '$qid'";
        $query = mysql_query($sql);
        Disconnect();
        return $query;
    }
?>

<?php
    include_once 'dbConnect.php';
    
    function AddMark($userId, $testId, $mark){
        Connect();
        $sql = "INSERT INTO studentmarks(`UserId`, `TestId`, `Mark`)
                VALUES('$userId', '$testId', '$mark')";
        $result = mysql_query($sql);
        $affectedrow = mysql_affected_rows();
        Disconnect();
        return $affectedrow;
    }
    
    function DeleteMark($id){
        Connect();
        $sql = "UPDATE studentmarks
                SET `Status`='0'
                WHERE `Id`='$id'";
        $result = mysql_query($sql);
        $affectedrow = mysql_affected_rows();
        Disconnect();
        return $affectedrow;
    }
    
    function SetMark($id, $userId, $testId, $mark, $status){
        Connect();
        $sql = "UPDATE studentmarks
                SET `UserId`='$userId', `TestId`='$testId', `Mark`='$mark', `Status`='$status'
                WHERE `Id`='$id'";
        $result = mysql_query($sql);
        $affectedrow = mysql_affected_rows();
        Disconnect();
        return $affectedrow;
    }
    
    function GetStudentMark($userId, $testId){
        Connect();
        $sql = "SELECT sm.`Id`, sm.`UserId`, sm.`TestId`, sm.`Mark`, us.`FirstName`, us.`LastName`, ts.`Name`
                FROM studentmarks sm
                JOIN users us
                    ON sm.`UserId` = us.`Id`
                JOIN tests ts
                    ON sm.`TestId` = ts.`Id`
                WHERE us.`Id`='$userId' AND ts.`Id` = '$testId'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function GetStudentMarkDateRange($userId, $testId, $startDate, $endDate){
        Connect();
        $sql = "SELECT sm.`Id`, sm.`UserId`, sm.`TestId`, sm.`Mark`, us.`FirstName`, us.`LastName`, ts.`Name`
                FROM studentmarks sm
                JOIN users us
                    ON sm.`UserId` = us.`Id`
                JOIN tests ts
                    ON sm.`TestId` = ts.`Id`
                WHERE us.`Id` = '$userId' AND ts.`Date` >= '$startDate' AND ts.`Date` <= '$endDate'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function GetStudentMarksLastExams($userId){
        Connect();
        /* Finding marks from last 10 exam */
        $sql = "SELECT sm.`Id`, sm.`UserId`, ts.`Id`, ts.`Name`, us.`FirstName`, us.`LastName`
                FROM studentmarks sm
                JOIN tests ts
                    ON sm.`TestId` = ts.`Id`
                JOIN users us
                    ON sm.`UserId` = us.`Id`
                WHERE sm.`UserId` = '$userId' AND sm.`TestId` IN ( SELECT DISTINCT ts.`Id` FROM tests ts ORDER BY ts.`Id` DESC LIMIT 10 )";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function GetStudentMarksExam($userId, $examId){
        Connect();
        $sql = "SELECT sm.`Id`, sm.`TestId`, sm.`Mark`, ts.`Name`
                FROM studentmarks sm
                JOIN tests ts
                    ON sm.`TestId` = ts.`Id`
                WHERE sm.`UserId` = '$userId' AND sm.`TestId` = '$examId' AND sm.`Status` = '1'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
?>

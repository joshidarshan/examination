<?php
    if(!isset($_SESSION))
        session_start();
    include_once 'dbConnect.php';
    
    function createUser($username, $password, $role, $firstName, $lastName, $classId, $divId){
        Connect();
        $sql = "INSERT INTO users(`UserName`, `Password`, `Role`, `FirstName`, `LastName`, `ClassId`, `DivId`, `Status`)
                VALUES('$username', '$password', '$role', '$firstName', '$lastName', '$classId', '$divId', '1')";
        $result = mysql_query($sql);
        $affectedRow = mysql_affected_rows();
        Disconnect();
        return $affectedRow;
    }
    
    function DeleteUser($uid){
        Connect();
        $sql = "UPDATE users
                SET `Status` = '0'
                WHERE `Id` = $uid";
        $result = mysql_query($sql);
        $affectedRow = mysql_affected_rows();
        Disconnect();
        return $affectedRow;
    }
    
    function SetUser($uid, $username, $password, $role, $firstName, $lastName, $classId, $divId, $status=1){
        Connect();
        $sql = "UPDATE users SET `UserName` = '$username', `Password` = '$password', `Role` = '$role', `FirstName` = '$firstName', `LastName` = '$lastName', `ClassId` = '$classId', `DivId` = '$divId', `Status` = '$status'
                WHERE `Id` = '$uid'";
        $result = mysql_query($sql);
        $affectedRow = mysql_affected_rows();
        Disconnect();
        return $affectedRow;
    }
    
    function GetStdUsers($stdId){
        Connect();
        $sql = "SELECT us.`Id`, us.`UserName`, us.`Role`, us.`FirstName`, us.`LastName`, us.`ClassId`, us.`DivId`, cl.`Name` as class, dv.`Name` as division
                FROM users us
                JOIN classes cl
                    ON us.`ClassId` = cl.`Id` AND us.`ClassId` = '$stdId' /*AND us.`Status` = '1'*/
                JOIN divisions dv
                    ON us.`DivId` = dv.`Id`";
        $query = mysql_query($sql);
        Disconnect();
        return $query;
    }
    
    
    
    function getUsersCount(){
        Connect();
        $sql = "SELECT COUNT(Id) FROM users";
        $result = mysql_query($sql);
        $row = mysql_fetch_row($result);
        Disconnect();
        return $row[0];
    }
    
    function getUserInfo($userId){
        Connect();
        $sql = "SELECT us.UserName, us.Password, us.Role, us.FirstName, us.LastName, cl.Name, di.`Name`, us.`ClassId` as classId, us.`DivId` as divId, us.`Status`
                FROM users us
                JOIN classes cl
                    ON us.ClassId = cl.Id
                JOIN divisions di
                    ON us.DivId = di.Id
                WHERE us.Id = $userId
                    /*AND us.Status = 1*/";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
        
    }
    
    function GetUserInfoUserName($username){
        Connect();
        $sql = "SELECT us.`Id`, us.UserName, us.Password, us.Role, us.FirstName, us.LastName, cl.Name, di.`Name`
                FROM users us
                JOIN classes cl
                    ON us.ClassId = cl.Id
                JOIN divisions di
                    ON us.DivId = di.Id
                WHERE us.`UserName` = '$username'
                    AND us.Status = 1";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function setUserInfo($Id, $Password, $Role, $FirstName, $LastName, $ClassId, $DivId, $Status){
        Connect();
        $sql = "UPDATE users SET 
                Password = '$Password',
                `Role` = '$Role', 
                `FirstName` = '$FirstName', 
                `LastName` = '$LastName', 
                `ClassId` = '$ClassId', 
                `DivId` = '$DivId', 
                `Status` = '$Status'
                WHERE Id = $Id";
        $result = mysql_query($sql);
        $affectedrow = mysql_affected_rows();
        Disconnect();
        return $affectedrow;
    }
    
    function LogIn($userName, $password){
        Connect();
        $sql = "SELECT `Id`, `FirstName`, `LastName`, `Role`
                FROM users 
                WHERE UserName = '$userName' AND Password = '$password'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function LogOff(){
        session_unset();
        session_cache_expire(-1);
        if(isset($_SESSION['UID']))
            unset ($_SESSION['UID']);
        session_destroy();
    }
    
    function IsLogIn(){
        if(isset($_SESSION['UID']))
            return TRUE;
        return FALSE;
    }    
?>

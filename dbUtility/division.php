<?php
    include_once 'dbConnect.php';
    
    function CreateDivision($name, $classId){
        Connect();
        $sql = "INSERT INTO divisions(`Name`, `ClassId`, `Status`)
                VALUES ('$name', '$classId', '1')";
        $result = mysql_query($sql);
        $affectedRow = mysql_affected_rows();
        Disconnect();
        return $affectedRow;
    }
    
    function DeleteDivision($id){
        Connect();
        $sql = "UPDATE divisions
                SET `Status` = '0'
                WHERE `Id` = $id";
        $result = mysql_query($sql);
        $affectedRow = mysql_affected_rows();
        Disconnect();
        return $affectedRow;
    }
    
    function GetAllDivisions(){
        Connect();
        $sql = "SELECT Id, Name
                FROM divisions
                WHERE `Status` = '1'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function GetAllClassDivision(){
        Connect();
        $sql = "SELECT cl.`Id` as clid, cl.`Name` as clname, di.`Id` as did, di.`Name` as diname, di.`Status` as Status
                FROM classes cl
                JOIN divisions di
                    ON cl.`Id` = di.`ClassId`
                ORDER BY cl.`Id`";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function GetDivision($id){
        Connect();
        $sql = "SELECT Name,
                FROM divisions
                WHERE `Id` = $id AND `Status` = '1'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function GetClassandDivision($divId){
        Connect();
        $sql = "SELECT cl.`Id` as clid, cl.`Name` as clname, di.`Id` as did, di.`Name` as diname, di.`Status` as Status
                FROM classes cl
                JOIN divisions di
                    ON cl.`Id` = di.`ClassId` AND di.`Id` = '$divId'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function GetClassDivision($classId){
        Connect();
        $sql = "SELECT `Id`, `Name`
                FROM divisions
                WHERE `ClassId` = '$classId' AND `Status` = '1'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
    
    function SetDivision($id, $name, $classId, $status){
        Connect();
        $sql = "UPDATE divisions
                SET `Name` = '$name', `ClassId` = '$classId', `Status` = '$status'
                WHERE `Id` = '$id'";
        $result = mysql_query($sql);
        Disconnect();
        return $result;
    }
?>
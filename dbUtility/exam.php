<?php
include_once 'dbConnect.php';

function GetExamDetails($id){
    Connect();
    $sql = "SELECT `Name`, `startFrom`, `endTo`, `timeLimit`, `positiveMark`, `negativeMark`, `classId`, status
            FROM exams
            WHERE `Id` = '$id' AND status = '1'";
    $result = mysql_query($sql);
    Disconnect();
    return $result;
}

function GetEditExamDetail($id){
    Connect();
    $sql = "SELECT `Name`, `startFrom`, `endTo`, `timeLimit`, `positiveMark`, `negativeMark`, `classId`, status
            FROM exams
            WHERE `Id` = '$id'";
    $result = mysql_query($sql);
    Disconnect();
    return $result;
}

function GetExamTime($userId, $examId){
    Connect();
    $sql = "SELECT `Id`, `timeRemain`
            FROM userexamappers
            WHERE `userId` = '$userId' AND `examId` = '$examId'";
    $result = mysql_query($sql);
    $data = mysql_fetch_row($result);
    $time = $data[1];
    Disconnect();
    return $time;
}

function GetStdExam($stdId){
    Connect();
    $stdExamSql = "SELECT `Id`, `Name`, `classId`, `startFrom`, `endTo`, `timeLimit`, `positiveMark`, `negativeMark`, status
                    FROM exams
                    WHERE `classId` = '$stdId' /*AND `status` = '1'*/";
    $stdQuery = mysql_query($stdExamSql);
    Disconnect();
    return $stdQuery;
}

function GetStdnameExam($stdId){
    Connect();
    $stdExamSql = "SELECT ex.`Id`, ex.`Name`, ex.`classId`, ex.`startFrom`, ex.`endTo`, ex.`timeLimit`, ex.`positiveMark`, ex.`negativeMark`, cl.`Name` as clname,  ex.status as Status
                    FROM exams ex
                    JOIN classes cl
                        ON ex.`classId` = cl.`Id` AND `classId` = '$stdId'";
    $stdQuery = mysql_query($stdExamSql);
    Disconnect();
    return $stdQuery;
}

function GetAvailableExam($usrId){
    Connect();
    
    // Gettting current users class, division
    $sql = "SELECT `ClassId`, `DivId`
            FROM users
            WHERE `Id` = '$usrId'";
    $userData = mysql_query($sql);
    $userStandardDiv = mysql_fetch_assoc($userData);
    $userStandard = $userStandardDiv['ClassId'];
    $userDiv = $userStandardDiv['DivId'];
    
    // Getting exam due in present two dates
    $startDate = date('Y-m-d');
    $endDate = date("Y-m-d", strtotime("+2 days"));
    $sql1 = "SELECT `Id`, `Name`
            FROM exams
            WHERE `startFrom` >='$startDate' AND `endTo` <='$endDate' AND status = '1' AND `classId` = '$userStandard'";
    /*$sql1 = "SELECT `Id`, `Name`
            FROM exams
            WHERE status = '1' AND `classId` = '$userStandard'";*/
    $data = mysql_query($sql1);
    $sourceExam = array();
    $sourceExamName = array();
    while($row = mysql_fetch_assoc($data)){
        $sourceExam[] = $row['Id'];
        $sourceExamName[$row['Id']] = $row['Name'];
    }    
    
    
    // Getting exam user has given and have been ended or time overed
    $sql2 = "SELECT `examId`
            FROM userexamappers
            WHERE `userId` = '$usrId' AND status = '0'";
    $data2 = mysql_query($sql2);
    $givenExams = array();
    while($row = mysql_fetch_assoc($data2)){
        $givenExams[] = $row['examId'];
    }
    
    $examList = array_diff($sourceExam, $givenExams);
    
    $finalExamList = array();
    foreach ($examList as $exm){
        $finalExamList[] = $exm;
    }
    
    $finalExamListWithName = array();
    foreach ($finalExamList as $key=>$val){
        $local_array['Name'] = $sourceExamName[$val];
        $local_array['Id'] = $val;
        
        array_push($finalExamListWithName, $local_array);
    }
    Disconnect();
    
    return $finalExamListWithName;
}

function GetTestSubjects($examId){
    Connect();
    $sql = "SELECT DISTINCT sb.`Name`, sb.`Id`
            FROM examquestions ex
            JOIN questions qs
                ON ex.`questionId` = qs.`Id`
            JOIN subjects sb
                ON qs.`subId` = sb.`Id`
            WHERE ex.`examId` = '$examId'";
    $result = mysql_query($sql);
    $subs = array();
    while($row = mysql_fetch_array($result)){
        $subs[$row['Id']] = $row['Name'];
    }
    Disconnect();
    return $subs;
}

function GetExamQuestions($examId){
    $esub = GetTestSubjects($examId);
    $questions = array();
    Connect();
    
    foreach($esub as $ek=>$ev){
        $sql = "SELECT qu.`Id`
                FROM questions qu
                JOIN examquestions eq
                    ON qu.`Id` = eq.`questionId` AND qu.`subId` = '$ek' AND eq.`examId` = '$examId'
                ORDER BY Rand()";
        
        $eqry = mysql_query($sql);
        
        while($erow = mysql_fetch_assoc($eqry)){
            $questions[] = $erow['Id'];
        }
    }
    
    // Old query
    /*$sql = "SELECT `questionId` AS `id`
            FROM examquestions
            WHERE `examId` = '$examId' and status = '1'
            ORDER BY questionId";
    $result = mysql_query($sql);
    
    while($row = mysql_fetch_assoc($result)){
        $questions[] = $row['id'];
    }*/
    
    
    Disconnect();
    return $questions;
}

function InsertExamQuestion($examId, $qid){
    Connect();
    $insExamQue = "INSERT INTO examquestions(`examId`, `questionId`, `status`)
                    VALUES('$examId', '$qid', '1')";
    $insExamData = mysql_query($insExamQue);
    $id = mysql_insert_id();
    Disconnect();
    return $id;
}

/*function GetQuestion($qid){
    Connect();
    $sql = "SELECT question
            FROM questions
            WHERE `Id` = '$qid'";
    $result = mysql_query($sql);
    $questionData = mysql_fetch_row($result);
    $question = $questionData[0];
    Disconnect();
    return $question;
}*/

function GetOptions($questionId, $userId, $examId){
    Connect();
    
    $sqlPreviousAnswer = "SELECT `Id`, `ansId`, `timeTaken`, `attempts`
                            FROM userexams uex
                            WHERE `userId` = '$userId' AND `examId` = '$examId' AND `questionId` = '$questionId'";
    $previousResult = mysql_query($sqlPreviousAnswer);
    $previousResultData = mysql_fetch_row($previousResult);
    $previousAnswerId = $previousResultData[1];
    
    
    $sql = "SELECT `Id`, `Option`
            FROM `options`
            WHERE `questionId` = '$questionId' AND status = '1'";
    $result = mysql_query($sql);
    $optionList="";
    while($row = mysql_fetch_assoc($result)){
        if($previousAnswerId == $row['Id']){
             $optionList .= '<ul style="width:auto; overflow:hidden;">
                        <li style="float:left; margin:0px 10px 0 0;">
                            <input id="option-'.$row['Id'].'" type="radio" name="option" value='.$row['Id'].' checked="checked" class="option"/>
                        </li>
                        <li style="width:auto; overflow:hidden; margin: 0px 0 0 10px; cursor:pointer;"><label for="option-'.$row['Id'].'">'.$row['Option'].'</label></li>
                     </ul>';
        }
        else{
            
                $optionList.='<ul style="width:auto; overflow:hidden;">
                        <li style="float:left; margin:0px 10px 0 0;">
                            <input id="option-'.$row['Id'].'" type="radio" name="option" value='.$row['Id'].' class="option"/>
                        </li>
                        <li style="width:auto; overflow:hidden; margin: 0px 0 0 10px; cursor:pointer;"><label for="option-'.$row['Id'].'">'.$row['Option'].'</label></li>
                     </ul>';
        }
    }
    
    Disconnect();
    return $optionList;
}

function PushTime($studentId, $examId, $time){
    Connect();
    $sql = "INSERT INTO userexamappers(`examId`, `userId`, `timeRemain`)
            VALUES('$examId', '$studentId', '$time')";
    $result = mysql_query($sql);
    $affectedrows = mysql_affected_rows();
    Disconnect();
    return $affectedrows;
}

function UpdateExamTime($uexId, $time){
    Connect();
    $time = intval($time) / 60;
    $time = intval($_SESSION['totalExamTime']) - $time;
    
    $sql = "UPDATE userexamappers
            SET `timeRemain` = '$time' WHERE `Id` = '$uexId'";
    $result = mysql_query($sql);
    $affectedrows = mysql_affected_rows();
    Disconnect();
    return $affectedrows;
}

function GetSectionWiseFirstQuestions($examId){
    $subjects = array();
	$subjectData = GetTestSubjects($examId);
    /*while($row = mysql_fetch_assoc($subjectData)){
        $subjects[] = $row['Id'];
    }*/
    foreach($subjectData as $key=>$val){
        $subjects[] = $key;
    }
    
    $subjectWiseFirstRow=array();
    Connect();
    foreach ($subjects as $sub){
        $sql = "SELECT qu.Id
                FROM subjects sb
                JOIN questions qu
                    ON qu.subId = sb.Id
                JOIN examquestions ex
                    ON ex.questionId = qu.Id
                WHERE sb.Id = '$sub' AND ex.examId = '$examId'";
        $data = mysql_query($sql);
        $firstRow = mysql_fetch_row($data);
        $subjectWiseFirstRow[$sub] = $firstRow[0];
        }
        Disconnect();
    return $subjectWiseFirstRow;
}

function AnswerQuestion($qid, $aid, $examId, $userId, $time, $isAttempt){
    Connect();
    $userExamData = $_SESSION['userData'];
    $currectQidData = array();
    /*
     * $local_array['qid'] = $row['qid'];
        $local_array['caid'] = $row['aid'];
        $local_array['uaid'] = 0;
        $local_array['time'] = 0;
        $local_array['attempts'] = 0;
     */
    foreach($userExamData as $key=>$value){
        if($value['qid'] == $qid){
            $currectQidData['qid'] = $value['qid'];
            $currectQidData['caid'] = $value['caid'];
            $currectQidData['uaid'] = $value['uaid'];
            $currectQidData['time'] = $value['time'];
            $currectQidData['attempts'] = $value['attempts'];
            
            break;
        }
    }
    // Old replaced with sp: GetPreviousAnswer(uId, eId, qId)
    /*$sqlPreviousAnswer = "SELECT `Id`, `ansId`, `timeTaken`, `attempts`
                            FROM userexams uex
                            WHERE `userId` = '$userId' AND `examId` = '$examId' AND `questionId` = '$qid'";
    
    $previousResult = mysql_query($sqlPreviousAnswer);*/
    
    // New replacement with sotred procedure
    Connect();
    $previousResult = mysql_query("CALL GetPreviousAnswer('$userId', '$examId', '$qid')");
    Disconnect();
    
    $previousResultData = mysql_fetch_row($previousResult);
    
    $visits = 0; $attempts = 1; $mark = $_SESSION['NegativeMarks']; $isCorrect = 0;
    
    // Check if answer is correct or not
    Connect();
    if(isset($aid)){
        // Old replaced with sp: GetCorrectAnswer($qid)
        /*$correctAnsSql = "SELECT `Id`
                          FROM `options`
                          WHERE `questionId` = '$qid' AND `isCorrect` = '1'";
        $correctAnsData = mysql_query($correctAnsSql);*/
        
        // New replacement with stored procedure
        $correctAnsData = mysql_query("CALL GetCorrectAnswer('$qid')");
        Disconnect();
        
        $correctAnswerResult = mysql_fetch_row($correctAnsData);
        $correctAnswer = $correctAnswerResult[0];
        if($aid == $correctAnswer){
            $isCorrect = 1;
            $mark = $_SESSION['PositiveMarks'];
        }
    }
    Connect();
    // If previously answer given and new answer is different then previous one then update that data with new details
    if($previousResultData){
        
        $previousAnswerId = $previousResultData[1];
        $previousTimeTaken = $previousResultData[2];
        $previousAttempts = $previousResultData[3];
        
        if($aid != $previousAnswerId){
            $visits = intval($previousAttempts) + 1;
            $time = $previousTimeTaken + $time;
            $attempts = $previousAttempts + 1;
            
            $updateSql = "UPDATE userexams
                        SET `ansId` = '$aid', `timeTaken` = '$time', `attempts` = '$attempts', mark = '$mark', `isAttempt` = '1', `isCorrect` = '$isCorrect', status = '1'
                        WHERE `userId` = '$userId' AND `examId` = '$examId' AND `questionId` = '$qid'";
            $updateResult = mysql_query($updateSql);
            Disconnect();
            return mysql_affected_rows();
        }
    }
    
    // If this is first time answer then insert new record
    else{
        if($aid){
            $insertSql = "INSERT INTO userexams(`userId`, `examId`, `questionId`, `ansId`, `isCorrect`, `isAttempt`, `mark`, `attempts`, `timeTaken`, `status`)
                         VALUES('$userId', '$examId', '$qid', '$aid', '$isCorrect', '$isAttempt', '$mark', '$attempts', '$time', '1')";
            $insertResult = mysql_query($insertSql);
            Disconnect();
            return mysql_affected_rows();
        }
    }
}

function NewAnswerQuestion($qid, $aid, $examId, $userId, $time, $isAttempt){
    Connect();
    $correctDataQuery = mysql_query("CALL GetCorrectAnswer('$qid')");
    Disconnect();
    $correctData=array();
    while($row = mysql_fetch_assoc($correctDataQuery)){
        $correctData[] = $row['Id'];
    }
    
    $marks = GetQuestionMark($qid);
    $marksData = mysql_fetch_assoc($marks);
    $positiveMark = $marksData['PositiveMark'];
    $negativeMark = $marksData['NegativeMark'];
    $userExamData = $_SESSION['userData'];
    $currectQidData = array();
    
    foreach($userExamData as $key=>$value){
        if($value['qid'] == $qid){
            $currectQidData['qid'] = $value['qid'];
            $currectQidData['caid'] = $value['caid'];
            $currectQidData['uaid'] = $value['uaid'];
            $currectQidData['time'] = $value['time'];
            $currectQidData['attempts'] = $value['attempts'];
            
            break;
        }
    }
    
    $visits = 0; $attempts = 1; $mark = $negativeMark; $isCorrect = 0;
    
    // Check if answer is correct or not
    if(isset($aid)){
        $correctAnswer = $currectQidData['caid'];
        
        // Old logic used for only one correct answer.
        /*if($aid == $correctAnswer){
            $isCorrect = 1;
            $mark = $_SESSION['PositiveMarks'];
        }*/
        
        // New logic used for as many correct answer as possible.
        foreach($correctData as $key=>$value){
            if($aid == $value){
                $isCorrect = 1;
                $mark = $positiveMark;
                break;
            }
        }
        
    }
    
    // If previously answer given and new answer is different then previous one then update that data with new details
    if($currectQidData['attempts'] != 0){
        $previousAnswerId = $currectQidData['uaid'];
        $previousTimeTaken = $currectQidData['time'];
        $previousAttempts = $currectQidData['attempts'];
        
        if($aid != $previousAnswerId){
            $visits = intval($previousAttempts)+1;
            $time = $previousTimeTaken+$time;
            $attempts = intval($previousAttempts)+1;
            
            // change answer into userarray aswell
            
            
            Connect();
            mysql_query("CALL UpdateUserAnswer('$aid', '$time', '$attempts', '$mark', '$isCorrect', '$userId', '$examId', '$qid')");
            $updateAffectedRows = mysql_affected_rows();
            Disconnect();
            return $updateAffectedRows;
        }
    }
    // If this is first time answer then insert new record
    else{
        if($aid){
            Connect();
            mysql_query("CALL InsertUserExamData('$userId', '$examId', '$qid', '$aid', '$isCorrect', '$isAttempt', '$mark', '$attempts', '$time')");
            $insertAffectedRows = mysql_affected_rows();
            Disconnect();
            return $insertAffectedRows;
        }
    }
    
    // Changing answer in session variable as well
    if($currectQidData['uaid'] != $aid){
        foreach($userExamData as $key=>$value){
                if($value['qid'] == $qid){
                    $userExamData[$key]['uaid'] = $aid;
                    $userExamData[$key]['time'] = $time;
                    $userExamData[$key]['attempts'] = $attempts;
                }
            }
        $_SESSION['userData'] = $userExamData;
    }
}

function UnanswerQuestion($qid, $examId, $userId){
    Connect();
    $sql = "UPDATE userexams
            SET `isCorrect` = '0', mark = '0', `ansId` = 0
            WHERE `questionId` = '$qid' AND `examId` = '$examId' AND `userId` = '$userId'";
    /*$sql = "UPDATE userexams
            SET `isAttempt` = '1', `isCorrect` = '0', mark = '0', `ansId` = 0
            WHERE `questionId` = '$qid' AND `examId` = '$examId' AND `userId` = '$userId'";*/
    $result = mysql_query($sql);
    $affectedrow = mysql_affected_rows();
    Disconnect();
    
    $ud = $_SESSION['userData'];
    foreach($ud as $key=>$value){
        if($value['qid'] == $qid){
            if(isset($ud[$key]['attampts']))
                $ud[$key]['attempts'] = intval($ud[$key]['attampts'])+1;
            else
                $ud[$key]['attempts'] = 1;
        }
    }
    $_SESSION['userData'] = $ud;
    return $affectedrow;
}

function GetExamTimeInFormat($time){
    $hour = intval($time / 60);
    $minute = intval($time - ($hour*60));
    $returnTime = $hour.":".$minute;
    return $returnTime;
}

function CreateExam($examName, $stdId, $posMark, $navMark, $time, $validFrom, $validTo, $uid){
    Connect();
    if($navMark>0)
        $navMark = -1 * $navMark;
    $createExamSql = "INSERT INTO exams(`Name`, `classId`, `startFrom`, `endTo`, `createdBy`, `timeLimit`, `positiveMark`, `negativeMark`, status)
                        VALUES('$examName','$stdId', '$validFrom', '$validTo', '$uid', '$time', '$posMark', '$navMark', '1')";
    $createdExamQuery = mysql_query($createExamSql);
    $examId = mysql_insert_id();
    Disconnect();
    return $examId;
}

function DeleteExam($examId){
    Connect();
    $sql = "UPDATE exams
            SET status = '0'
            WHERE `Id` = '$examId'";
    $query = mysql_query($sql);
    $affectedrow = mysql_affected_rows();
    Disconnect();
    return $affectedrow;
}

function SetExam($examId, $examName, $classId, $startFrom, $endTo, $timeLimit, $posMark, $nagMark, $uid, $status){
    Connect();
    $date = date('Y-m-d H:i:s');
    $sql = "UPDATE exams
            SET `Name` = '$examName', `classId` = '$classId', `startFrom` = '$startFrom', `endTo` = '$endTo', `modifyBy` = '$uid', `modifyOn` = '$date', `timeLimit` = '$timeLimit', `positiveMark` = '$posMark', `negativeMark` = '$nagMark', status = '$status'
            WHERE `Id` = '$examId'";
    $query = mysql_query($sql);
    $affectedrow = mysql_affected_rows();
    Disconnect();
    return $affectedrow;
}

function GetSubChpWiseQuestionCount($subId, $chpId, $count){
    Connect();
    $questionIdSql = "SELECT count(qu.`Id`)
                        FROM questions qu
                        JOIN subjects sb
                            ON qu.`subId` = sb.`Id`
                        WHERE qu.`chapterId` = '$chpId' AND sb.`Id` = '$subId'";
    $questionIdQuery = mysql_query($questionIdSql);
    $questionData = mysql_fetch_row($result);
    $questionCount = $questionData[0];
    Disconnect();
    return $questionCount;
}

function InsertExamQuestionSubChpWise($subId, $chpId, $count){
    Connect();
    $questionIdSql = "SELECT qu.`Id`
                        FROM questions qu
                        JOIN subjects sb
                            ON qu.`subId` = sb.`Id`
                        WHERE qu.`chapterId` = '$chpId' AND sb.`Id` = '$subId'";
    $questionIdQuery = mysql_query($questionIdSql);
    $questionIdList = array();
    while($row = mysql_fetch_assoc($questionIdQuery))
            $questionIdList[] = $row['Id'];
    
    Disconnect();
    
    $min=0;$max=count($questionIdList)-1;
    $finalQid = array();
    
    if(count($questionIdList) != $count){
        if(count($questionIdList) < $count)
            $count = count($questionIdList);
    }
    
    for($i=0;$i<$count;){
        $rndVal = rand($min, $max);
        $isAdded = false;
        if(isset($finalQid) && !empty($finalQid)){
            foreach ($finalQid as $row=>$val){
                if($val != $questionIdList[$rndVal]){
                    $isAdded = FALSE;
                }
                else
                    $isAdded = TRUE;
                
                if($isAdded == FALSE){
                    $finalQid[] = $questionIdList[$rndVal];
                    $i++;
                }
                else
                    continue;
            }
        }
        else{
            $finalQid[] = $questionIdList[$rndVal];
            $i++;
        }
    }
    return $finalQid;
}

function GetLastAnsForExam($userId, $examId){
    Connect();
    $queryResult = mysql_query("CALL GetUserAnswerForExam('$userId', '$examId')");
    $userAnsId = array();
    while($row = mysql_fetch_assoc($queryResult)){
        
    }
    Disconnect();
}

function PrepareUserExamData($userId, $examId){
    // Function needs to get shuffle question according to subject.
    // Preparing basic userdata: qid, aid, caid, time, attempts
    Connect();
    $queryResult = mysql_query("CALL GetExamQuestionCorrectAnsId('$examId')");
    $userData = array();
    while($row = mysql_fetch_assoc($queryResult)){
        $local_array['qid'] = $row['qid'];
        $local_array['caid'] = $row['aid'];
        $local_array['uaid'] = 0;
        $local_array['time'] = 0;
        $local_array['attempts'] = 0;
        
        array_push($userData, $local_array);
    }
    Disconnect();
    
    // Getting user's exam perfrom data
    Connect();
    $userDataResult = mysql_query("CALL GetExamUserPerformData('$examId', '$userId')");
    $userPerformData = array();
    while($udrow = mysql_fetch_assoc($userDataResult)){
        $ud_array['qid'] = $udrow['questionId'];
        $ud_array['uaid'] = $udrow['uaid'];
        $ud_array['time'] = $udrow['time'];
        $ud_array['attempts'] = $udrow['attempts'];
        
        array_push($userPerformData, $ud_array);
    }
    Disconnect();
    
    // finalizing user's data array
    foreach($userData as $key=>$value){
        foreach($userPerformData as $upkey=>$upvalue){
            if($upvalue['qid'] == $value['qid']){
                $userData[$key]['uaid'] = $upvalue['uaid'];
                $userData[$key]['time'] = $upvalue['time'];
                $userData[$key]['attempts'] = $upvalue['attempts'];
            }
        }
    }
    $_SESSION['userData'] = $userData;
    return $userData;
}

function UserExamPerformData($uid, $examId){
    // select the data if available
    Connect();
    $prevSelSql = "SELECT `Id`, `timeRemain`, attempts, status
                    FROM userexamappers
                    WHERE `userId` = '$uid' AND `examId` = '$examId'";
    $prevSelQuery = mysql_query($prevSelSql);
    Disconnect();
    $prevSelData = mysql_fetch_assoc($prevSelQuery);
    
    $returnData = 0;
    
    // If time is almost zero then exam will be disabled
    if(isset($prevSelData['Id']) && !empty($prevSelData['Id'])){
        $id = $prevSelData['Id'];
        if($prevSelData['timeRemain'] <= 1){
            Connect();
            $returnData = -1;
            $disableExamSql = "UPDATE userexamappers
                                SET status = '0', `timeRemain` = '0'
                                WHERE `Id` = '$id'";
            mysql_query($disableExamSql);
            Disconnect();
        }
    }
    
    // If user have appear for the exam then changes the attempt count
    if(isset($prevSelData['Id']) && !empty($prevSelData['Id'])){
        Connect();
        $attempt = intval($prevSelData['attempts']) + 1;
        $id = $prevSelData['Id'];
        
        $attUpdateSql = "UPDATE userexamappers
                            SET `attempts` = '$attempt'
                            WHERE `Id` = '$id'";
        $attUpdateQuery = mysql_query($attUpdateSql);
        if(mysql_affected_rows() > 0)
            $returnData = $id;
        Disconnect();
        
    }
    else{
        $examData = GetExamDetails($examId);
        $examInfo = mysql_fetch_assoc($examData);
        
        $timeRemain = $examInfo['timeLimit'];
        $posMark = $examInfo['positiveMark'];
        $navMark = $examInfo['negativeMark'];
        
        //SELECT `Name`, `startFrom`, `endTo`, `timeLimit`, `positiveMark`, `negativeMark`
        Connect();
        $attInsertSql = "INSERT INTO userexamappers(`examId`, `userId`, `timeRemain`, `attempts`, `status`)
                        VALUES('$examId', '$uid', '$timeRemain', '1', '1')";
        $attInsertQuery = mysql_query($attInsertSql);
        if($attInsertQuery)
            $returnData = mysql_insert_id ();
        Disconnect();
    }
    return $returnData;
}

function GetSubjectWiseExamScore($userId, $examId){
    Connect();
    $subWiseScoreSql = "SELECT SUM(ue.mark) as Mark, sb.`Name`
                        FROM userexams ue
                        JOIN questions qu
                            ON ue.`questionId` = qu.`Id` AND ue.`status` = '1' AND ue.`isAttempt` = '1' AND ue.`examId` = '$examId' AND ue.`userId` = '$userId'
                        JOIN subjects sb
                            ON qu.`subId` = sb.`Id`
                        GROUP BY sb.`Name`";
    $subWiseScoreData = mysql_query($subWiseScoreSql);
    Disconnect();
    $scoreData = array();
    while($row = mysql_fetch_assoc($subWiseScoreData)){
        $scoreData[$row['Name']] = $row['Mark'];
    }
    return $scoreData;
}

function EndExam($userId, $examId){
    Connect();
    $endExamSql = "UPDATE userexamappers
                    SET `status` = 0
                    WHERE `userId` = '$userId' AND `examId` = '$examId'";
    $endExamQuery = mysql_query($endExamSql);
    $affectedrows = mysql_affected_rows();
    Disconnect();
    return $affectedrows;
}

function StudentLastExam($userId){
    Connect();
    $sql = "SELECT `examId`
            FROM userexamappers
            WHERE `userId` = '$userId'
            ORDER BY `examId` desc
            LIMIT 0, 1";
    $qry = mysql_query($sql);
    $data = mysql_fetch_row($qry);
    Disconnect();
    return $data[0];
}

function GetStdLastExam($stdId){
    Connect();
    $sql = "SELECT `Id`, `Name`, `classId`, `startFrom`, `endTo`, `timeLimit`, status
            FROM exams
            WHERE `classId` = '$stdId'
            ORDER BY `Id` DESC
            LIMIT 0, 1";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}


?>

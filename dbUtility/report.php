<?php

include_once 'dbConnect.php';

function GetStdStudentResult($stdId, $divId) {
    Connect();
    $lastExamSql = "SELECT ex.`Id`
                    FROM exams ex
                    WHERE ex.`classId` = '$stdId' AND ex.status = '1'
                    ORDER BY ex.`Id` DESC";
    $data = mysql_query($lastExamSql);
    $lastExamResult = mysql_fetch_row($data);
    $lastExamId = $lastExamResult[0];

    // student list for division
    $divisionStudent = "SELECT `Id`, `UserName`, `FirstName`, `LastName`
                        FROM users
                        WHERE `ClassId` = '$stdId' AND `DivId` = '$divId'";
    $divStudentData = mysql_query($divisionStudent);
    $studentList = array();
    $studentListForQuery = "";
    $counter = 0;
    while ($row = mysql_fetch_assoc($divStudentData)) {
        $studentList[$row['Id']] = $row['FirstName'] . " " . $row['LastName'];
        if ($counter == 0)
            $studentListForQuery .= $row['Id'];
        else
            $studentListForQuery .= ", " . $row['Id'];
        $counter++;
    }

    // Get total marks of exam
    $examMarksSql = "SELECT COUNT(eq.`questionId`) as total, `positiveMark`, ex.`Name`
                    FROM exams ex
                    JOIN examquestions eq
                        ON ex.`Id` = eq.`examId`
                    WHERE ex.`Id` = '$lastExamId'";
    $exmMarkData = mysql_query($examMarksSql);
    $examMarksData = mysql_fetch_row($exmMarkData);
    $examTotalMark = $examMarksData[0] * $examMarksData[1];
    $examName = $examMarksData[2];

    // exam result according to student
    $studentMarkSql = "SELECT SUM(`mark`) AS Mark, `userId`
                        FROM userexams
                        GROUP BY `userId`, `examId`
                        HAVING `userId` in ($studentListForQuery) AND `examId` = '$lastExamId'";
    $studentMarkData = mysql_query($studentMarkSql);
    $studentWiseMarks = array();
    while ($row = mysql_fetch_assoc($studentMarkData)) {
        $studentWiseMarks[$row['userId']] = $row['Mark'];
    }

    // Prepare json response here
    $returnData = array();
    foreach ($studentWiseMarks as $key => $val) {
        $row_array['Id'] = $key;
        $row_array['Name'] = $studentList[$key];
        $row_array['Mark'] = $val;
        $row_array['TotalMark'] = $examTotalMark;
        $row_array['ExamName'] = $examName;
        $row_array['ExamId'] = $lastExamId;

        array_push($returnData, $row_array);
    }
    Disconnect();
    return $returnData;
}

function GetStudentAllExamResult($userId) {
    Connect();
    $sql = "SELECT ex.Id as examId, ex.startFrom, ex.endTo, ex.Name, sum(ue.mark) as Mark
            FROM userexams ue
            JOIN exams ex
                ON ue.examId = ex.Id AND ue.userId = '$userId'
            GROUP BY ex.Id, ex.Name
            ORDER BY examId DESC";
    $examData = mysql_query($sql);
    $StudentAllExamResult = array();
    while ($row = mysql_fetch_assoc($examData)) {
        $row_array['ExamId'] = $row['examId'];
        $row_array['Name'] = $row['Name'];
        $row_array['Mark'] = $row['Mark'];
        $row_array['TimeRange'] = $row['startFrom'] . "   &nbsp;&nbsp; To &nbsp;&nbsp;   " . $row['endTo'];

        array_push($StudentAllExamResult, $row_array);
    }
    Disconnect();
    return $StudentAllExamResult;
}

function GetStudentResultExam($userId, $examId) {
    Connect();
    /*$examMarkSql = "SELECT sum(ue.mark) as Mark, count(ue.`questionId`), ex.`positiveMark`, ex.`negativeMark`
                FROM userexams ue
                JOIN exams ex
                    ON ue.`examId` = ex.`Id`
                WHERE ue.`userId` = '$userId' AND ue.`examId` = '$examId' ";*/
    $examMarkSql = "SELECT COUNT(eq.Id), ex.`positiveMark`
                    FROM examquestions eq
                    JOIN exams ex
                        ON eq.`examId` = ex.`Id`
                    WHERE `examId` = '$examId'";
    $examMarkQuery = mysql_query($examMarkSql);
    $examMarkData = mysql_fetch_row($examMarkQuery);
    $mark = $examMarkData[0];
    $totalMarks = $examMarkData[0] * $examMarkData[1];

    $usermarkSql = "SELECT SUM(mark) as Mark
                    FROM userexams
                    WHERE `userId` = '$userId' AND `examId` = '$examId'";
    $usermarkQuery = mysql_query($usermarkSql);
    $usermarkData = mysql_fetch_assoc($usermarkQuery);
    $mark = $usermarkData['Mark'];
    
    $examAttendCount = "SELECT COUNT(`Id`)
                        FROM userexams
                        WHERE `isAttempt` = '1' AND `userId` = '$userId' AND `examId` = '$examId' AND status = '1'";
    $examAttendQuery = mysql_query($examAttendCount);
    $examAttendData = mysql_fetch_row($examAttendQuery);
    $attemptedQuestion = $examAttendData[0];

    /*$examUnAttendCount = "SELECT COUNT(`Id`)
                        FROM userexams
                        WHERE `isAttempt` = '0' AND `userId` = '$userId' AND `examId` = '$examId'";
    $examUnAttendQuery = mysql_query($examUnAttendCount);
    $examUnAttendData = mysql_fetch_row($examUnAttendQuery);
    $UnAttemptedQuestion = $examUnAttendData[0];*/
    $UnAttemptedQuestion = $examMarkData[0] - $attemptedQuestion;

    $correctQuestionCount = "SELECT COUNT(`Id`)
                        FROM userexams
                        WHERE `isCorrect` = '1' AND `userId` = '$userId' AND `examId` = '$examId' AND status = '1'";
    $correctQuestionQuery = mysql_query($correctQuestionCount);
    $correctQuestionData = mysql_fetch_row($correctQuestionQuery);
    $correctQuestion = $correctQuestionData[0];

    $inCorrectQuestionCount = "SELECT COUNT(`Id`)
                        FROM userexams
                        WHERE `isCorrect` = '0' AND `userId` = '$userId' AND `examId` = '$examId' AND status = '1'";
    $inCorrectQuestionQuery = mysql_query($inCorrectQuestionCount);
    $inCorrectQuestionData = mysql_fetch_row($inCorrectQuestionQuery);
    $inCorrectQuestion = $inCorrectQuestionData[0];

    $resultData = array();
    $resultData['Marks'] = $mark;
    $resultData['TotalMarks'] = $totalMarks;
    $resultData['Attempt'] = $attemptedQuestion;
    $resultData['UnAttempt'] = $UnAttemptedQuestion;
    $resultData['Correct'] = $correctQuestion;
    $resultData['InCorrect'] = $inCorrectQuestion;

    return $resultData;
    Disconnect();
}

function ReviewExam($userId, $examId) {
    Connect();
    $reviewData = array();
    // Geting exam question, option
    $examSql = "SELECT qu.`Id` as qid, qu.question as question, op.`Option` as opt
                FROM examquestions ex
                JOIN questions qu
                    ON ex.`questionId` = qu.`Id` AND ex.`examId` = '$examId'
                JOIN `options` op
                    ON qu.`Id` = op.`questionId` AND op.`isCorrect` = '1'";
    $examQuery = mysql_query($examSql);
    while ($row = mysql_fetch_assoc($examQuery)) {
        $toAdd = TRUE;
        foreach ($reviewData as $key => $value) {
            if(!empty($reviewData)){
                if ($value['qid'] == $row['qid']){
                    $reviewData[$key]['correctAns'] = $reviewData[$key]['correctAns'] . ", " . $row['opt'];
                    $toAdd = FALSE;
                }
            }
        }
    if($toAdd) {
        $local_array['qid'] = $row['qid'];
        $local_array['question'] = $row['question'];
        $local_array['correctAns'] = $row['opt'];
        $local_array['answer'] = '0';
        $local_array['correct'] = '0';
        $local_array['attampt'] = '0';
        $local_array['visit'] = '0';
        $local_array['time'] = '0';
        $local_array['mark'] = '0';

        array_push($reviewData, $local_array);
        }
    }
    
    // finding the user's data for the exam
    $userExamSql = "SELECT ue.`questionId` as qid, op.`Option` as opt, ue.`isCorrect` as correct, ue.`isAttempt` as attempt, ue.attempts as visit, ue.`timeTaken` as time, ue.mark as mark
                    FROM userexams ue
                    JOIN `options` op
                        ON ue.`ansId` = op.`Id`
                    WHERE ue.`examId` = '$examId' AND ue.`userId` = '$userId'";
    $userExamQuery = mysql_query($userExamSql);
    while($row = mysql_fetch_assoc($userExamQuery)){
        foreach ($reviewData as $key=>$value){
            if($value['qid'] == $row['qid']){
                $reviewData[$key]['answer'] = $row['opt'];
                $reviewData[$key]['correct'] = $row['correct'];
                $reviewData[$key]['attampt'] = $row['attempt'];
                $reviewData[$key]['visit'] = $row['visit'];
                $reviewData[$key]['time'] = $row['time'];
                $reviewData[$key]['mark'] = $row['mark'];
            }
        }
    }
    Disconnect();
    return $reviewData;
}

function GetLastExamSectionWiseScore($userid, $examId) {
    Connect();
    $lasExamSectionScoreSql = "SELECT sb.`Name`, SUM(ue.mark) as Mark, ue.userId, ue.examId
                                FROM userexams ue
                                JOIN questions qu
                                  ON ue.`questionId` = qu.`Id` AND ue.`userId` = '$userid' AND ue.`examId` = '$examId'
                                JOIN subjects sb
                                  ON qu.`subId` = sb.`Id`
                                GROUP BY sb.`Name`";
    $lastExamSectionScoreQuery = mysql_query($lasExamSectionScoreSql);
    $lastExamSectionData = array();
    while ($row = mysql_fetch_assoc($lastExamSectionScoreQuery)) {
        $new_array['Name'] = $row['Name'];
        $new_array['Mark'] = $row['Mark'];

        array_push($lastExamSectionData, $new_array);
    }
    Disconnect();
    return $lastExamSectionData;
}

function GetStudentExamPerformanceByTime($userId, $examId) {
    Connect();
    $studentExamPerformByTimeSql = "SELECT SUM(ue.`timeTaken`)/60 as Time, sb.`Name`
                                    FROM userexams ue
                                    JOIN questions qu
                                        ON ue.`questionId` = qu.`Id` AND ue.`userId` = '$userId' AND ue.`examId` = '$examId'
                                    JOIN subjects sb
                                        ON qu.`subId` = sb.`Id`
                                    GROUP BY sb.`Name`";
    $studentExamPerformQuery = mysql_query($studentExamPerformByTimeSql);
    Disconnect();
    return $studentExamPerformQuery;
}

function GetStudentLastExamsResult($userId) {
    Connect();
    $lastExamSql = "SELECT ue.`examId`
            FROM userexamappers ue
            WHERE ue.`userId` = '$userId'
            ORDER BY ue.`examId` DESC
            LIMIT 0, 10";
    $lastExamData = mysql_query($lastExamSql);
    $lastExamIds = "0";
    while ($row = mysql_fetch_assoc($lastExamData))
        $lastExamIds .= ", " . $row['examId'];

    $lastExamPerformanceSql = "SELECT ue.`examId`, ex.`Name`, sum(ue.mark) as Mark
                                FROM userexams ue
                                JOIN exams ex
                                    ON ue.`examId` = ex.`Id` AND ue.`examId` IN ($lastExamIds) AND ue.`userId` = '$userId'
                                GROUP BY ex.`Name`";
    $lastExamPerformanceData = mysql_query($lastExamPerformanceSql);
    Disconnect();
    return $lastExamPerformanceData;
}

function GetStudentResultTimeRange($userId, $startFrom, $endTo) {
    Connect();
    $timeExamSql = "SELECT ue.`examId`
                    FROM userexamappers ue
                    JOIN exams ex 
                        ON ue.examId = ex.Id
                    WHERE ue.`userId` = '1' AND ue.`userId` = '$userId' AND ex.startFrom >= '$startFrom' AND ex.endTo <= '$endTo'
                    ORDER BY ue.`examId` DESC
                    LIMIT 0 , 10";
    $timeExamData = mysql_query($timeExamSql);
    $timeExamId = "0";
    while ($row = mysql_fetch_assoc($timeExamData))
        $timeExamId .= ", " . $row['examId'];

    $timeExamPerformanceSql = "SELECT ue.`examId`, ex.`Name`, sum(ue.mark) as Mark
                                FROM userexams ue
                                JOIN exams ex
                                    ON ue.`examId` = ex.`Id` AND ue.`examId` IN ($timeExamId) AND ue.`userId` = '$userId'
                                GROUP BY ex.`Name`";
    $timeExamPerformanceData = mysql_query($timeExamPerformanceSql);
    $examData = mysql_fetch_assoc($timeExamPerformanceData);
    Disconnect();
    return $examData;
}

function GetStdLastExamResult($examId){
    Connect();
    // getting result of all users for that standard
    $sqlUserExamPerform = "SELECT SUM(ue.mark) as Mark,  ue.`userId`, us.`FirstName`, us.`LastName`
                            FROM userexams ue
                            JOIN users us
                                ON ue.`userId` = us.`Id` AND ue.`examId` = '$examId'
                            GROUP BY ue.`userId`
                            LIMIT 0, 1000";
    $qryUserExamPerform = mysql_query($sqlUserExamPerform);
    Disconnect();
    return $qryUserExamPerform;
}

function GetStdExamTopStudents($examId){
    Connect();
    $sql = "SELECT SUM(ue.mark) as Mark, us.`FirstName`, us.`LastName`
            FROM userexams ue
            JOIN users us
                ON ue.`userId` = us.`Id` AND ue.`examId` = '$examId'
            GROUP BY ue.`userId`
            ORDER BY Mark DESC
            LIMIT 0, 10";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}
?>

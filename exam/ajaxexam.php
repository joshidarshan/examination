<?php
session_start();
include_once '../dbUtility/exam.php';
include_once '../dbUtility/Question.php';

/*
 * url: "ajaxexam.php",
   data: "qid="+qid+"&aid="+answerId+"&q=answer&originalqid="+originalqid,
 */

// detecting the type of request: Answer, Unanswer, Skip
$type = $_GET['q'];
$qidArray = $_SESSION['qid'];
$uid = $_SESSION['UID'];
$uaexId = $_SESSION['uaex'];
$examId = $_SESSION['examId'];
$qid = $qidArray[$_GET['qid']-1];
$originalqid = $qid;
if($_GET['qid'] > 1 )
    $originalqid = $qidArray[$_GET['qid'] - 2];
$time = strtotime(date("Y-m-d H:i:s")) - strtotime($_SESSION['lastTime']);

// Push remaining time into examapper
UpdateExamTime($uaexId, $time);

if($type == 'answer'){
    $aid = $_GET['aid'];
    NewAnswerQuestion($originalqid, $aid, $examId, $uid, $time, 1);
}
else if($type == 'unanswer'){
    UnanswerQuestion($originalqid, $examId, $uid);
}
else if($type == 'navigate'){
    //$qid = $_GET['qid'];
}
else if($type == 'last'){
    $aid = $_GET['aid'];
    NewAnswerQuestion($qid, $aid, $examId, $uid, $time, 1);
}
else if($type == 'mark'){
    //$qid = $_GET['qid'];
}

$question = GetQuestion($qid);
$option = GetOptions($qid, $uid, $examId);

$respone = array();
$respone['question'] = $question;
$respone['option'] = $option;
$respone['qid'] = $qid;
$_SESSION['lastTime'] = date("Y-m-d H:i:s");

echo json_encode($respone);
//echo $return = "{\"question\":$question, \"option\":\"$option\",\"qid\":\"$qid\"}";
?>

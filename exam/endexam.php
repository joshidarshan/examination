<?php
session_start();
//include_once '../templets/clienttemplate.php';
include_once '../dbUtility/exam.php';

// deactivating exam for student.
EndExam($_SESSION['UID'], $_SESSION['examId']);
//$data = GetSubjectWiseExamScore($_SESSION['UID'], $_SESSION['examId']);
?>
<META HTTP-EQUIV="refresh" CONTENT="3;URL=../examListing.php">
<div class="contaner">
    Your examination is completed successfully. 
    <!--Your score is following:
    
    <table width="60%" style="line-height: 40px;">
        <tr>
            <th>Subject</th>
            <th>Score</th>
        </tr>
    
    <?php
    /*foreach($data as $key=>$value){
        echo "<tr>";
        echo "<td>$key</td>";
        echo "<td>$value</td>";
        echo "</tr>";
    }*/
    ?>
    </table>-->
    You can review exam tomorrow.
</div>

<?php
include_once '../templets/footerTemplate.php';
?>

<?php
    unset($_SESSION['examId']);
    unset($_SESSION['lastTime']);
    unset($_SESSION['NegativeMarks']);
    unset($_SESSION['PositiveMarks']);
    unset($_SESSION['uaex']);
    unset($_SESSION['totalExamTime']);
    unset($_SESSION['qid']);
    unset($_SESSION['userData']);
?>
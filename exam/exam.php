<?php
session_start();

if (!isset($_SESSION['UID']))
    header("Location: ../login.php");
if (!isset($_POST) && empty($_POST))
    header("Location: ../examListing.php?err=1");

include_once '../dbUtility/exam.php';
include_once '../dbUtility/Question.php';

//var_dump($_SESSION['userData']);

if (!empty($_POST)) {
    $examId = $_POST['exam'];

    $examQuery = GetExamDetails($examId);
    $examData = mysql_fetch_assoc($examQuery);

    // Inserting/Updating userexamapper for the exam and user
    // If it returns negative value exam should not get ahead
    $pastAppearanceId = UserExamPerformData($_SESSION['UID'], $examId);
    if ($pastAppearanceId < 0)
        header("Location: examerror.php");
    $_SESSION['uaex'] = $pastAppearanceId;


    //$_SESSION['username'] = 'Darshan Joshi';
    $_SESSION['examId'] = $examId;
    $_SESSION['lastTime'] = date("Y-m-d H:i:s");
    $_SESSION['NegativeMarks'] = $examData['negativeMark'];
    $_SESSION['PositiveMarks'] = $examData['positiveMark'];


    $_SESSION['totalExamTime'] = GetExamTime($_SESSION['UID'], $_SESSION['examId']);
    PrepareUserExamData($_SESSION['UID'], $_SESSION['examId']);
    $qid = GetExamQuestions($examId);
    $_SESSION['qid'] = $qid;

    $subjects = GetTestSubjects($examId);
    $timeForExam = GetExamTimeInFormat($_SESSION['totalExamTime']);

    //$subjectWiseFirstQuestion = GetSectionWiseFirstQuestions(2);
    $firstQuestion = GetQuestion($qid[0]);
    $firstOption = GetOptions($qid[0], 1, $examId);

    //header("Location: /education/exam/exam.php");
    $uqansData = $_SESSION['userData'];
    $uqansString = "";
    $attemptCount = 0;
    foreach ($uqansData as $key => $value) {
        $uqansString .= $value['uaid'] . ",";
        if ($value['uaid'] != 0)
            $attemptCount += 1;
    }
    //var_dump($_SESSION);
}
else {
    header("Location: ../examlisting.php");
}
?>
<html>
    <head>
        <title>Exam</title>
        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="timer.js"></script>
        <link rel="stylesheet" href="exam.css" />
    </head>

    <body style="margin: 0px; padding: 0px; background-color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 10pt;">
        <div id="hider" style="width:100%; height: 100%; background-color: black; opacity: 0.50; z-index: 500; position: fixed; top: 0; left: 0; display: none;">&nbsp;</div>
            <div style="width: 100%; margin: 0 auto;">
                <div style="width: auto; overflow: hidden; padding: 0 0 40px 0px;">
                    <div style="float: left; color: #345895; font-family: Levenim MT; font-size: 22px; margin: 0px 0 0 0px; padding: 10px 0 0 50px; text-transform: uppercase; font-weight: bold;">Test: </div>
                    <div style="float: right; margin-right: 32px; /*background-image: url(images/test/header-right.jpg);*/ background-repeat: repeat-x; background-color: #e9e9e9; border: 1px #d1d1d1 solid; border-radius: 0px 0px 4px 4px; -webkit-border-radius: 0px 0px 4px 4px; -moz-border-radius: 0px 0px 4px 4px; padding: 5px 10px 5px 0px;">
                        <div style="float: left; background-color: #fbfbfb; padding: 3px 8px; margin: 0 10px 0 10px; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border: 1px #c2c2c2 solid; font-size: 11px;">
                            <ul style="width: auto; overflow: hidden;">
                                <li style="float: left; margin: 1px 0 0 2px; vertical-align: top; padding: 0px;">Time Remaining </li>
                                <li style="float: left; margin: 0px 0 0 5px; vertical-align: top; padding: 0px;"><span id="timer" class="time-test">:&nbsp;59:59</span></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div style="width: 96%; margin: -38px auto 0px auto;">
                    <div style="width: 96%; position: absolute; margin: 523px 0 0 0px; z-index: -1;">
                        <div style="width: auto; height: 72px; background-repeat: no-repeat;">
                            <div style="width: auto; height: 72px; background-color: #ffffff; background-position: top right; background-repeat: no-repeat; padding: 0 10px;">
                                <div style="width: auto; background-color: #FFF;">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="width: auto; background-repeat: no-repeat;">
                        <div style="width: auto; background-position: top right; background-repeat: no-repeat; padding: 0 10px;">
                            <div style="width: auto; background-color: #FFF;">
                                <div style="width: auto; overflow: hidden;">
                                    <div style="float: left; width: 85%; margin: 0 0 0 10px;">
                                        <div style="width: auto; background-repeat: repeat-x; border: 1px #dddddd solid; border-radius: 4px 4px 0px 0px; -webkit-border-radius: 4px 4px 0px 0px; -moz-border-radius: 4px 4px 0px 0px; overflow: hidden; padding: 4px 0 0 0px;">
                                            <!--question start-->
                                            <div style="float: left; background-color:green; font-weight: bold; background-repeat: repeat-x; border-left: 1px #c9c9c9 solid; border-right: 1px #c9c9c9 solid; border-top: 1px #c9c9c9 solid; border-radius: 4px 4px 0px 0px; -webkit-border-radius: 4px 4px 0px 0px; -moz-border-radius: 4px 4px 0px 0px; padding: 4px 8px; color: #FFF; margin: 0 0 0 17px" id="serialNoDiv">Q. <span id="sid">1</span><span id="qid" style="display: none;">1</span></div>
                                            <div class="tab-box">
                                                <ul>
                                                    <?php
                                                    $count = 1;
                                                    foreach ($subjects as $key => $val) {
                                                        if ($count == 1)
                                                            echo '<li id="' . $val . '" class="tab-box1 tab-box2"><a id="" href="#"><span>' . $val . '</span></a></li>';
                                                        else
                                                            echo '<li id="' . $val . '" class="tab-box1"><a id="" href="#"><span>' . $val . '</span></a></li>';
                                                        $count++;
                                                    }
                                                    ?>
                                                    <!--<li id="cont-1-1" class="tab-box1"> <a id="" onclick="onTabClick(1);" href="Reasoning/0"><span>Reasoning</span></a></li>
                                                    <li id="cont-1-41" class="tab-box1 tab-box2"><a id="" onclick="onTabClick(41);" href="../English/0"><span>English</span></a></li>
                                                    <li id="cont-1-81" class="tab-box1"><a id="" onclick="onTabClick(81);" href="../Math/0"><span>Numerical Ability</span></a></li>
                                                    <li id="cont-1-121" class="tab-box1"><a id="" onclick="onTabClick(121);" href="../GK/0"><span>General Awareness</span></a></li>
                                                    <li id="cont-1-161" class="tab-box1"><a id="" onclick="onTabClick(161);" href="../Computer/0"><span>Computer Knowledge</span></a></li>-->
                                                </ul>
                                            </div>
                                        </div>

                                        <div style="z-index: 4; position: fixed; top: 35%; left: 38%; display: none;" id="preloader"></div>

                                        <div style="width: auto; font-size: 18px; border-left: 1px #f0f0f0 solid; border-right: 1px #f0f0f0 solid; height: 485px; padding: 0px 20px 0px 20px; overflow: auto;">
                                            <div id="conot-1" class="tabcontent">
                                                <form method="post" action="" id="testForm" name="testForm">
                                                    <div id="questiondivMain" style="width: auto; font-size: 18px; padding: 0px 0px 20px; display: block;">
                                                        <ul style="width: auto;">
                                                            <li style="width: auto; margin: 20px 0 0 0px;" id="questionDiv">
                                                                <p>
                                                                <div id="questionLabel">
                                                                    <?php echo $firstQuestion ?><br />
                                                                </div>
                                                                </p>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div id="formdiv">
                                                        <div style="width:auto; margin:10px 0 0 10px;" id="optionContainer">
                                                            <!--<ul style="width:auto; overflow:hidden;">
                                                                <li style="float:left; margin:0px 10px 0 0;">
                                                                    <input id='option-1' type="radio" name="option" value="1" checked="checked" class="option"/>
                                                                </li>
                                                                <li style="width:auto; overflow:hidden; margin: 0px 0 0 10px; cursor:pointer;"><label for="option-1">Option-1</label></li>
                                                            </ul>-->
                                                            <?php echo $firstOption ?>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div style="width: auto; background-color: #e8e9e9; border: 1px #dddddd solid; border-radius: 0px 0px 4px 4px; -webkit-border-radius: 0px 0px 4px 4px; -moz-border-radius: 0px 0px 4px 4px; overflow: hidden; padding: 8px 0 8px 0px;">
                                            <div id="nextID" class="previous" style="display: block;"><button value="Next" name="Next" onclick="return QuestionActivity('answer', '');" style="cursor: pointer;">Save & Next</button></div>
                                            <div class="previous-q"><button style='cursor: pointer;' onclick="return QuestionActivity('mark', '');" >Mark for Review</button></div>
                                            <div class="previous-q"><button style="cursor: pointer;" onclick="return QuestionActivity('skip', '');">Skip</button></div>
                                            <div class="previous-q"><button style='cursor: pointer;' onclick="return QuestionActivity('unanswer', '');" >Reset</button></div>
                                            <div class="Questions"><a style="cursor: pointer;"onclick="return QuestionActivity('end', '');">Submit Test</a></div>
                                        </div>
                                    </div>

                                    <div style="float: right; width: 12%; margin: 22px 10px 0 0px;">
                                        <div style="width: auto; background-color: #f2f3f3; font-weight: bold; padding: 10px 10px; border: 1px #e5e5e5 solid; border-radius: 4px 4px 0px 0px; -webkit-border-radius: 4px 4px 0px 0px; -moz-border-radius: 4px 4px 0px 0px;">Review Questions</div>
                                        <div style="width: auto; height: 425px; overflow: auto; border-left: 1px #e5e5e5 solid; border-right: 1px #CCC solid;" id="scrolldiv1">
                                            <div id="scrolldiv" class="select-question">
                                                <?php
                                                $counter = 1;
                                                $queData = $_SESSION['userData'];

                                                /*
                                                 * $local_array['qid'] = $row['qid'];
                                                  $local_array['caid'] = $row['aid'];
                                                  $local_array['uaid'] = 0;
                                                  $local_array['time'] = 0;
                                                  $local_array['attempts'] = 0;
                                                 */
                                                /* foreach($queData as $key=>$value){
                                                  echo '<ul>';
                                                  for($j=0; $j<4; $j++, $counter++){
                                                  if($value['uaid'] != 0)
                                                  echo "<li class='green' id='$value[qid]'><a onclick='return Navigate(".$value['qid'].");' style='cursor:pointer;'>".$counter."</a></li>";
                                                  else
                                                  echo "<li class='white' id='$value[qid]'><a onclick='return Navigate(".$value['qid'].");' style='cursor:pointer;'>".$counter."</a></li>";
                                                  }
                                                  echo '</ul>';
                                                  } */

                                                for ($i = 0; $i < count($qid);) {
                                                    echo '<ul>';
                                                    for ($j = 0; $j < 4; $j++, $counter++) {
                                                        if ($i >= count($qid))
                                                            break;
                                                        if ($queData[$i]['uaid'] != 0) {
                                                            //old //echo "<li class='green' id='$qid[$i]'><a onclick='return Navigate(".$qid[$i].");' style='cursor:pointer;'>".$counter."</a></li>";
                                                            //echo "<li class='green' id='q" . intval($i + 1) . "'><a onclick='return QuestionActivity(\'navigate\',\'\');' style='cursor:pointer;'>" . $counter . "</a>";
                                                            echo "<li class='green' id='q".intval($i+1)."'><a  onclick=QuestionActivity('navigate','".intval($i+1)."');>" . $counter . "</a>";
                                                        }
                                                        else
                                                            echo "<li class='white' id='q".intval($i+1)."'><a  onclick=QuestionActivity('navigate','".intval($i+1)."');>" . $counter . "</a>";
                                                        $i++;
                                                    }
                                                    echo '</ul>';
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <div style="width: auto; background-color: #999; background-color: #fafafb; padding: 0px 0 0px 5px; border-top: 1px #e5e5e5 solid; border-bottom: 1px #e5e5e5 solid; border-left: 1px #e5e5e5 solid; border-right: 1px #CCC solid;">
                                            <ul style="width: auto; overflow: hidden; display: none;">
                                                <li style="float: left; text-align: left; margin: 0 0 0 10px; padding: 5px 0px; font-weight: bold; width: 67%; border-right: 1px #e7e7e8 solid;">Attempted</li>
                                                <li style="float: left; text-align: left; margin: 0 0 0 10px; padding: 5px 0px; font-weight: bold;"><span id="attemptedH" style="color: #75b975;">0</span></li>
                                            </ul>
                                        </div>

                                        <div style="width: auto; background-color: #999; background-color: #fafafb; padding: 0px 0 0px 5px; border-bottom: 1px #e5e5e5 solid; border-left: 1px #e5e5e5 solid; border-right: 1px #CCC solid; border-radius: 0px 0px 4px 4px; -webkit-border-radius: 0px 0px 4px 4px; -moz-border-radius: 0px 0px 4px 4px;">
                                            <ul style="width: auto; overflow: hidden; display: none;">
                                                <li style="float: left; text-align: left; margin: 0 0 0 10px; padding: 5px 0px; font-weight: bold; width: 67%; border-right: 1px #e7e7e8 solid;">Unattempted</li>
                                                <li style="float: left; text-align: left; margin: 0 0 0 10px; padding: 5px 0px; font-weight: bold;"><span id="unAttemptedH" style="color: #cc2200;">100</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="index" name="index" value="1">
            <input type="hidden" id="attemptCount" name="attemptCount" value="<?php echo $attemptCount; ?>">
            <script type="text/javascript">
                window.onload = CreateTimer("timer", '<?php echo $timeForExam . ":00"; ?>');
                var idx = 0;
                
                function QuestionActivity(type, qid){
                    document.getElementById('hider').style['display'] = 'block';
                    
                    // Count total no of questions
                    var qidCount = '<?php echo count($qid); ?>';
                    
                    // Gets the selection option
                    var answerId = $('input[name=option]:checked').val();
                    
                    // Variable to maintain original qid index.
                    var originalqid = qid;
                    
                    // Variable to manage original index
                    var originalIdx = $("#index").val();
                    
                    // Appending original index with q for question navigation class changes.
                    originalIdx = "q" + originalIdx;

                    
                    
                    // If qid is null or not.
                    if (!qid) {
                        qid = $("#index").val();
                        originalqid = qid;
                        if (type == 'answer' || type == 'unanswer' || type == 'skip') {
                            qid = parseInt(qid);
                            qidCount = parseInt(qidCount);
                            if (qid >= qidCount) {
                                if (answerId) {
                                    if(type == 'unanswer'){
                                        qid = parseInt(qid) + 1;
                                        $(':radio').prop('checked', false);
                                        $.ajax({
                                            type: "GET",
                                            url: "ajaxexam.php",
                                            data: "qid=" + qid + "&q=unanswer&originalqid=" + originalqid,
                                            success: function(data) {
                                                try{
                                                    var responseData = jQuery.parseJSON(data);
                                                    $("#questionLabel").html(responseData.question);
                                                    $("#optionContainer").html(responseData.option);
                                                    $("#qid").text(responseData.qid);
                                                    $("#hider").css("display", "none");
                                                }
                                                catch(err){
                                                    $("#hider").css("display", "none");
                                                }
                                            },
                                            error: function(a, b, c){
                                                $("#hider").css("display", "none");
                                            }
                                        });
                                    }
                                    else{
                                        $.ajax({
                                            type: "GET",
                                            url: "ajaxexam.php",
                                            data: "qid=" + qid + "&aid=" + answerId + "&q=last&originalqid=" + originalqid,
                                            success: function(data) {
                                                $("#hider").css("display", "none");
                                            },
                                            error: function(a, b, c){
                                                $("#hider").css("display", "none");
                                            }
                                        });
                                    }
                                }
                                if (type == 'mark')
                                    $("#" + originalIdx).attr("class", "blue");
                                else if (type == 'answer')
                                    $("#" + originalIdx).attr("class", "green");
                                else if (type == 'unanswer')
                                    $("#" + originalIdx).attr("class", "white");
                                alert("Last question reached!");
                                return false;
                            }
                            else {
                                qid = parseInt(qid) + 1;
                                idx = $("#index").val();
                                idx = parseInt(idx) + 1;
                                $("#index").val(idx);
                                //$("#sid").text(idx);
                            }
                        }
                    }

                    if (type == 'answer') {
                        if (answerId) {
                            $("#" + originalIdx).attr("class", "green");
                            //$("#hider").css("display", "block");
                            //document.getElementById('hider').style['display'] = 'block';
                            $.ajax({
                                type: "GET",
                                url: "ajaxexam.php",
                                data: "qid=" + qid + "&aid=" + answerId + "&q=answer&originalqid=" + originalqid,
                                success: function(data) {
                                    var responseData = jQuery.parseJSON(data);
                                    $("#questionLabel").html(responseData.question);
                                    $("#optionContainer").html(responseData.option);
                                    $("#qid").text(responseData.qid);
                                    $("#hider").css("display", "none");
                                    $("#sid").text(idx);
                                },
                                error: function(data){
                                    $("#hider").css("display", "none");
                                }
                            });
                            //document.getElementById('hider').style['display'] = 'none';
                        }
                        else {
                            alert("Kindly answer the question to save ansswer or skip the question");
                            return false;
                        }
                    }
                    if (type == 'unanswer') {
                        $("#" + originalIdx).attr("class", "white");
                        $(':radio').prop('checked', false);
                        $("#hider").css("display", "block");
                        $.ajax({
                            type: "GET",
                            url: "ajaxexam.php",
                            data: "qid=" + qid + "&q=unanswer&originalqid=" + originalqid,
                            success: function(data) {
                                var responseData = jQuery.parseJSON(data);
                                $("#questionLabel").html(responseData.question);
                                $("#optionContainer").html(responseData.option);
                                $("#qid").text(responseData.qid);
                                $("#hider").css("display", "none");
                                $("#sid").text(idx);
                            },
                            error: function(data){
                                $("#hider").css("display", "none");
                            }
                        });
                        
                    }
                    if (type == 'skip') {
                        /*if (answerId)
                            $("#" + originalIdx).attr("class", "green");*/
                        $("#hider").css("display", "block");
                        $.ajax({
                            type: "GET",
                            url: "ajaxexam.php",
                            data: "qid=" + qid + "&q=navigate&originalqid=" + originalqid,
                            success: function(data) {
                                var responseData = jQuery.parseJSON(data);
                                $("#questionLabel").html(responseData.question);
                                $("#optionContainer").html(responseData.option);
                                $("#qid").text(responseData.qid);
                                $("#hider").css("display", "none");
                                $("#sid").text(idx);
                            },
                            error: function(data){
                                $("#hider").css("display", "none");
                            }
                        });
                    }
                    if (type == 'navigate') {
                        $("#hider").css("display", "block");
                        $.ajax({
                            type: "GET",
                            url: "ajaxexam.php",
                            data: "qid=" + qid + "&q=navigate&originalqid=" + originalqid,
                            success: function(data) {
                                var responseData = jQuery.parseJSON(data);
                                $("#questionLabel").html(responseData.question);
                                $("#optionContainer").html(responseData.option);
                                $("#qid").text(responseData.qid);
                                $("#sid").text(qid);
                                $("#index").val(qid);
                                $("#hider").css("display", "none");
                                $("#sid").text(qid);
                            },
                            error: function(data){
                                $("#hider").css("display", "none");
                            }
                        });
                    }
                    if (type == 'mark') {
                        $("#" + originalIdx).attr("class", "blue");
                        $("#hider").css("display", "none");
                    }
                    if (type == 'end') {
                        window.location = "endexam.php";
                    }
                }
                /*function Navigate(qid) {
                    qid = $("#index").val();
                    var originalqid = qid;
                    $("#hider").attr("display", "block");
                    $("#index").val(qid);
                    $("#sid").text(qid);
                    $.ajax({
                        type: "GET",
                        url: "ajaxexam.php",
                        data: "qid=" + qid + "&q=navigate&originalqid=" + originalqid,
                        success: function(data) {
                            var responseData = jQuery.parseJSON(data);
                            $("#questionLabel").html(responseData.question);
                            $("#optionContainer").html(responseData.option);
                            $("#qid").text(responseData.qid);
                        }
                    });
                    $("#hider").attr("display", "none");
                }*/
            </script>
    </body>
</html>
var Timer;
var Hour = 0, Minute = 0, Second = 0;

function CreateTimer(TimerID, Time) {
    Timer = TimerID;
    var SplitData = Time.split(":");
    Hour = parseInt(SplitData[0]);
    Minute = parseInt(SplitData[1]);
    Second = parseInt(SplitData[2]);

    UpdateTimer();
    window.setTimeout("Tick()", 1000);
}

function Tick() {
    if (Hour <= 0 && Minute <= 0 && Second <= 0) {
        alert("Done");
        window.location.replace("endexam.php");
        /*window.close();
        $(function () {
            $.ajax({
                url: "Exam/AjaxExamId",
                type: "GET",
                dataType: "json",
                success: function (data) {
                    alert("time is over, you may review your answers from ReviewExam tab");
                    window.close();
                }
            });
        });*/
        return;
    }
    Second -= 1;
    UpdateTimer();
    window.setTimeout("Tick()", 1000);
}

function UpdateTimer() {
    if (Second <= 0) {
        if (Minute > 0) {
            Minute -= 1;
            Second = 59;
        }
    }
    if (Minute <= 0) {
        if (Hour > 0) {
            Hour -= 1;
            Minute = 59;
        }
    }
    var TimerStr = LeadingZero(Hour) + ":" + LeadingZero(Minute) + ":" + LeadingZero(Second);
    document.getElementById("timer").innerHTML = TimerStr;
}

function LeadingZero(Time) {
    return (Time < 10) ? "0" + Time : +Time;
}
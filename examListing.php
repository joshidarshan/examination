<?php
    //session_start();
    
    if(isset($_GET['err']) && !empty($_GET['err']))
        if($_GET['err'] == 1)
            echo "Please select an exam to start";
    
    include_once './templets/clienttemplate.php';
    include_once './dbUtility/exam.php';
    include_once './dbUtility/Question.php';
    
    $data = GetAvailableExam($_SESSION['UID']);
    $examId = 0;
?>
<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript">
        function checkSubmit(){
            var answerId = $('input[name=exam]:checked').val();
            if(answerId){
                return true;
            }
            else{
                alert ('Kindly select exam');
                return false;
            }
        }
</script>

<form action="exam/newexam.php" method="POST" onsubmit="return checkSubmit();">
    <div style="clear:both; margin-top: 25px;">&nbsp;

        <h1 style='text-align: center;'>Select Exam</h1>

        <table class="table table-striped">
            <tr>
                <th>Select</th>
                <th>Serial</th>
                <th>Name</th>
            </tr>
            <?php
            foreach($data as $key=>$value){
                echo "<tr>";
                echo "<td><input type='radio' name='exam' value=$value[Id]>";
                echo "<td>$value[Id]</td>";
                echo "<td>$value[Name]</td>";
                echo "</tr>";
            }
            ?>
        </table>

        <div style="clear:both;" class="btn-group">
            <input type="submit" class="btn-large" name="submit" value="submit">
        </div>
    </div>
</form>
<?php
include_once './templets/clientfooter.php';
?>

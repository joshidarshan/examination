<?php
    $error = false;
    $url="";
    if(!empty($_GET))
        if(isset($_GET['error']))
            $error = $_GET['error'];
        else if(isset($_GET['url']))
            $url=$_GET['url'];
?>
<!doctype html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> 	<html lang="en"> <!--<![endif]-->
    <head>

        <!-- General Metas -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	<!-- Force Latest IE rendering engine -->
        <title>Education Tracking | Login Form</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 

        <!-- Stylesheets -->
        <link rel="stylesheet" href="style/base.css">
        <link rel="stylesheet" href="style/skeleton.css">
        <link rel="stylesheet" href="style/layout.css">
    </head>

    <body>
        <div class="notice" style="display: <?php if($error)echo 'block'; else echo 'none'; ?>">
            <a href="" class="close">close</a>
            <p class="warn">Whoops! We didn't recognise your username or password. Please try again.</p>
        </div>

        <div class="container">
            <div class="form-bg">
                <form method="POST" action="utility/login.php?url=<?php echo $url; ?>">
                    <h2>Login</h2>
                    <p><input type="text" name="userName" placeholder="Username"></p>
                    <p><input type="password" name="password" placeholder="Password"></p>
                    <label for="remember">
                        <input type="checkbox" id="remember" value="remember" />
                        <span>Remember me on this computer</span>
                    </label>
                    <button type="submit" value="Login"></button>
                </form>
            </div>
        </div><!-- container -->
        <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    </body>
</html>
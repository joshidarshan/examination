Issues:
1: Optimize exam: Load all question at first, and only push answer to db if it's new or updated from previous.
A: Made session variable that manages qid, correct-ansid, user-ansid for faster retrival. 
Now exam takes 7-9 second to load and question takes 3-4 sec to load.

2: insertquestion: check if code has any bug
A: No bug found so far. Recheck for new updates.

3: examListing: check if code has any bug
A: Works correct.

4: Loading image: Put loading image while new question is being rendered via ajax.
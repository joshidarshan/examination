<?php
/* *code to handle postback* */
?>

<?php include_once './clienttemplate.php'; ?>
<form method="POST" action="result.php">
    <div class="contaner">
        <div class="contaner_top-cpr">
            <span style="font-size:40px">Client Parents results</span>
        </div>
        <div class="contaner_index-cpr">
            <div class="range-cpr">
                <div class="range_heading-cpr">
                    <span style="font-size:40px">Range</span>
                </div>
                <div class="range_det-cpr">
                    <div class="range_detail-cpr">
                        <table>
                            <tr>
                                <td>From</td> <td>:</td> <td><input type="date" name="date" placeholder="Date" /></td>
                            </tr>
                            <tr>
                            </tr>
                            <tr>
                                <td>To</td> <td>:</td> <td><input type="date" name="date" placeholder="Date" /></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="testtype-cpr">
                <div class="testtype_heading-cpr">
                    <span style="font-size:40px">Test Type</span>
                </div>
                <div class="testtype_det-cpr">
                    <div class="testtype_detail-cpr">
                        <select class="Test Type">
                            <option>Unit</option>
                            <option>Semester</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="subsel-cpr">
                <div class="subsel_heading-cpr">
                    <span style="font-size:40px">Subject Select</span>
                </div>
                <div class="subsel_det-cpr">
                    <div class="subsel_detail-cpr">
                        <select class="Subject Select">
                            <option>English</option>
                            <option>Math</option>
                            <option>Science</option>
                        </select>
                    </div>    
                </div>
            </div>
        </div>
        <div class="contaner_show-cpr">
            <div class="testname-cpr">
                <div class="testname_heading-cpr">
                    <span style="font-size:40px">Test Name</span>
                </div>
                <div class="testname_det-cpr">
                    T1_SCIENCE
                </div>
            </div>
            <div class="sub-cpr">
                <div class="sub_heading-cpr">
                    <span style="font-size:40px">Subject</span>
                </div>
                <div class="sub_det-cpr">
                    SCIENCE
                </div>
            </div>
            <div class="date-cpr">
                <div class="date_heading-cpr">
                    <span style="font-size:40px">Date</span>
                </div>
                <div class="date_det-cpr">
                    20APR2013
                </div>
            </div>
            <div class="avg-cpr">
                <div class="avg_heading-cpr">
                    <span style="font-size:38px">Average(marks)</span>
                </div>
                <div class="avg_det-cpr">
                    90%
                </div>
            </div>
        </div>
        <div class="contaner_chart-cpr">
            <img src="image/Science-Marks.jpg" style="width:900px; height:230px;" />
        </div>
    </div>
</form>
<?php include_once './clientfooter.php'; ?>
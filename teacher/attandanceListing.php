<?php
/* code to handle post events */
?>

<?php include_once '../templets/teacherTemplateHeader.php'; ?>
<form method="POST" action="userupdate.php">
    <div class="contaner">
        <div class="contaner_top-teacherattingandancelist">
            <div class="subject-tal">
                <span style="font-size:40px">Teacher Attandance Listing</span>
            </div>
            <div class="contaner_top_class-tal">
                <div class="class1-tal">
                    <p><b>CLASS - 1</b></p>
                </div>
                <div class="class2-tal">
                    <p><b>CLASS - 2</b></p>
                </div>
            </div>
        </div>
        <div class="contaner_bottom-tal">
            <div class="contaner_bottom_left-tal">
                <div class="contaner_bottom_left_score-tal">
                    <span style="font-size:40px; text-shadow:0px 2px 3px #555">SCORE</span>
                </div>
                <div class="contaner_bottom_left_report-tal">
                    <div class="contaner_bottom_left_report_sub-tal">
                        <div class="contaner_bottom_left_report_sub_heading-tal">
                            <h2>SUBJECT</h2>
                            <p>ENGLISH</p>
                            <p>MATH</p>
                            <p>S.S.</p>
                            <p>SCIENCE</p>
                        </div>
                        <div class="contaner_bottom_left_report_sub_name-tal">
                        </div>
                    </div>
                    <div class="contaner_bottom_left_report_test-tal">
                        <div class="contaner_bottom_left_report_test_heading-tal">
                            <h2>TEST</h2>
                            <p>T1_ENGLISH</p>
                            <p>T1_MATH</p>
                            <p>T1_S.S.</p>
                            <p>T1_SCIENCE</p>
                        </div>
                        <div class="contaner_bottom_left_report_test_name-tal">
                        </div>
                    </div>
                    <div class="contaner_bottom_left_report_avg-tal">
                        <div class="contaner_bottom_left_report_avg_heading-tal">
                            <h2>AVERAGE</h2>
                            <p>78%</p>
                            <p>65%</p>
                            <p>58%</p>
                            <p>85%</p>

                        </div>
                        <div class="contaner_bottom_left_report_avg_marks-tal">
                        </div>
                    </div>
                </div>
            </div>
            <div class="contaner_bottom_right-tal">
                <div class="contaner_bottom_right_top-tal">
                    <span style="font-size:40px; text-shadow:0px 2px 3px #555">TOP - 10</span>
                </div>
                <div class="contaner_bottom_right_topstud-tal">
                    <p>STUDENT - 1</p>
                    <p>STUDENT - 2</p>
                    <p>STUDENT - 3</p>
                    <p>STUDENT - 4</p>
                    <p>STUDENT - 5</p>
                    <p>STUDENT - 6</p>
                    <p>STUDENT - 7</p>
                    <p>STUDENT - 8</p>
                    <p>STUDENT - 9</p>
                    <p>STUDENT - 10</p>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include_once '../templets/footerTemplate.php'; ?>
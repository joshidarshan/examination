<?php
/* code to handle submission */
?>

<?php include_once '../templets/teacherTemplateHeader.php'; ?>
<form method="POST" action="testMark.php">
    <div class="contaner">
        <div class="contaner_top-ttm">
            <div class="subject">
                <span style="font-size:40px">Teacher Test Marks</span>
            </div>
            <div class="contaner_top_middle-ttm">
                <div class="class-ttm">
                    <div class="classadj-ttm">
                        <span style="font-size:24px">CLASS</span>
                        <select class="CLASS" style="width:125px">
                            <option>Class</option>
                            <option>1st</option>
                            <option>2nd</option>
                            <option>3rd</option>
                            <option>4th</option>
                        </select>
                    </div>
                </div>
                <div class="division-ttm">
                    <div class="divisionadj-ttm">
                        <span style="font-size:24px">DIVISION</span>
                        <select class="DIVISION" style="width:125px">
                            <option>Division</option>
                            <option>A</option>
                            <option>B</option>
                            <option>C</option>
                            <option>D</option>
                        </select>
                    </div>
                </div>
                <div class="test-ttm">
                    <div class="testadj-ttm">
                        <span style="font-size:24px">TEST</span>
                        <select class="TEST" style="width:125px">
                            <option>Test</option>
                            <option>Sem - 1</option>
                            <option>Sem - 2</option>
                            <option>Sem - 3</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="contaner_top_bottom-ttm">
                <div class="class1-ttm">
                    <h3>TEST - 1</h3>
                </div>
                <div class="class2-ttm">
                    <h3>TEST - 2</h3>
                </div>
            </div>
        </div>
        <div class="contaner_bottom-ttm">
            <div class="contaner_bottom_left-ttm">
                <div class="rollno-ttm">
                    <span style="font-size:40px; text-shadow:0px 2px 3px #555" >ROLL NO</span>
                </div>
                <div class="rollnodata-ttm">
                    <p>1</p>
                </div>
            </div>
            <div class="contaner_bottom_middle-ttm">
                <div class="name-ttm">
                    <span style="font-size:40px; text-shadow:0px 2px 3px #555">NAME</span>
                </div>
                <div class="namedata-ttm">
                    <p>STUDENT - 1</p>
                </div>
            </div>
            <div class="contaner_bottom_right-ttm">
                <div class="marks-ttm">
                    <span style="font-size:40px;text-shadow:0px 2px 3px #555">MARKS</span>
                </div>
                <div class="marksdate-ttm">
                    <p><input type="text" name="marks" placeholder="marks" /></p>
                </div>
            </div>
        </div>
        <div class="submit-ttm">
            <input type="button" name="submit" value="SUBMIT" style="width:100px" />
        </div>
    </div>
</form>
<?php include_once '../templets/footerTemplate.php'; ?>
<?php
    if (!isset($_SESSION)) {session_start();}
    if(!isset($_SESSION['UID']))
        header("Location: ../Login.php?url=admin/index.php");
    
    include_once '../dbUtility/User.php';
    $userId = $_SESSION['UID'];
    $userInfo = "";
    if($userId)
        $userInfo = getUserInfo($userId);
    
    $selCategory = "";
    if(isset($_SESSION) && !empty($_SESSION))
        $selCategory = $_SESSION['cat'];
    
?>
<!DOCTYPE html>   
<html lang="en">   
    <head>   
        <meta charset="utf-8">   
        <title>Education Management System | Analytics System</title>   
        <meta name="description" content="Twitter Bootstrap tab based dropdown Navigation Example">  
        <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">  
        <style type="text/css">   
            .container {  
                /*width: auto;
                margin-left: 10px;
                text-align: center;*/
            }
        </style>  
    </head>  
    <body>
        <div class="navbar navbar-fixed-top">  
            <div class="navbar-inner">  
                <div class="container" style="width: auto; margin-left: 20px;">  
                    <ul class="nav nav-tabs">  
                        <li>
                            <a class="brand" href="../admin/index.php">SV-Kadi</a>
                        </li>
                        
                        <li <?php if(isset($selCategory) && !empty($selCategory) && $selCategory == 'home'){echo "class='active'";} else{ echo "class=''";}?> ><a href="#">Home</a></li>  
                        <li <?php if(isset($selCategory) && !empty($selCategory) && $selCategory == 'test'){echo "class='dropdown active'";} else{ echo "class='dropdown'";}?> >  
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Test<b class="caret"></b></a>  
                            <ul class="dropdown-menu">  
                                <li><a href="../admin/examlisting.php">List Exam</a></li>
                                <li><a href="#">Create Test</a></li>  
                                <li><a href="#">Create Test With Que</a></li>  
                                <li class="divider"></li>  
                                <li><a href="#">Edit Test</a></li>  
                            </ul>  
                        </li>  
                        
                        <li <?php if(isset($selCategory) && !empty($selCategory) && $selCategory == 'report'){echo "class='dropdown active'";} else{ echo "class='dropdown'";}?> >  
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Report<b class="caret bottom-up"></b></a>  
                            <ul class="dropdown-menu bottom-up pull-right">  
                                <li><a href="../admin/report.php">Class & Division</a></li>  
                                <li><a href="../admin/reportclass.php">Class Wise Report</a></li>
                                <li class="divider"></li>  
                                <li><a href="../admin/reportprint.php">Print Report</a></li>  
                            </ul>  
                        </li>  
                        <li <?php if(isset($selCategory) && !empty($selCategory) && $selCategory == 'user'){echo "class='dropdown active'";} else{ echo "class='dropdown'";}?> >  
                            <a class="dropdown-toggle" data-toggle="dropdown" href="../admin/userlisting.php">User<b class="caret bottom-up"></b></a>  
                            <ul class="dropdown-menu bottom-up pull-right">
                                <li><a href="../admin/userlisting.php">List User</a></li>
                                <li><a href="../admin/newuser.php">Create User</a></li>  
                                <li class="divider"></li>  
                                <li><a href="../admin/userimport.php">Import User</a></li>  
                            </ul>  
                        </li>
                        <li <?php if(isset($selCategory) && !empty($selCategory) && $selCategory == 'question'){echo "class='dropdown active'";} else{ echo "class='dropdown'";}?> >  
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Question Option<b class="caret bottom-up"></b></a>  
                            <ul class="dropdown-menu bottom-up pull-right">  
                                <li><a href="../admin/insertquestion.php">Insert question & option</a></li>  
                                <li class="divider"></li>  
                                <li><a href="../admin/questionoptionimport.php">Import Question & Option</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Print Question & Option</a></li>
                            </ul>  
                        </li>
                        <li <?php if(isset($selCategory) && !empty($selCategory) && $selCategory == 'class'){echo "class='dropdown active'";} else{ echo "class='dropdown'";}?> >  
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Class & Division<b class="caret bottom-up"></b></a>  
                            <ul class="dropdown-menu bottom-up pull-right">  
                                <li><a href="../admin/stdlisting.php">List Class</a>
                                <li><a href="../admin/stdcreate.php">Create Class</a></li>
                                <li class="divider"></li>
                                <li><a href="../admin/divlisting.php">List Division</a></li>
                                <li><a href="../admin/divisioncreate.php">Create Division</a></li>
                                <li class="divider"></li>  
                                <li><a href="#">Print Class & Division</a></li>  
                            </ul>  
                        </li>
                        <li <?php if(isset($selCategory) && !empty($selCategory) && $selCategory == 'subject'){echo "class='dropdown active'";} else{ echo "class='dropdown'";}?> >  
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Subject<b class="caret bottom-up"></b></a>  
                            <ul class="dropdown-menu bottom-up pull-right">  
                                <li><a href="../admin/sublisting.php">List Subject</a></li>
                                <li><a href="../admin/subjectcreate.php">Create Subject</a></li>  
                                <li class="divider"></li>  
                                <li><a href="#">Print Subject</a></li>  
                            </ul>  
                        </li>
                        <li <?php if(isset($selCategory) && !empty($selCategory) && $selCategory == 'chapter'){echo "class='dropdown active'";} else{ echo "class='dropdown'";}?> >  
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Chapter<b class="caret bottom-up"></b></a>  
                            <ul class="dropdown-menu bottom-up pull-right">  
                                <li><a href="../admin/chplisting.php">List Chapter</a></li>
                                <li><a href="../admin/chaptercreate.php">Create Chapter</a></li>  
                                <li class="divider"></li>  
                                <li><a href="#">Print Chapter</a></li>  
                            </ul>  
                        </li>
                        <li><a href="../logout.php">LogOff</a></li>
                    </ul>        
                </div>  
            </div>  
        </div>  
        <script src="../js/jquery-1.9.1.js"></script>  
        <script src="../bootstrap/js/bootstrap.js"></script>  
        <div style="margin-top: 35px;">&nbsp;
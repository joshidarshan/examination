<?php
    if (!isset($_SESSION)) {session_start();}
    if(!isset($_SESSION['UID']))
        header("Location: ../Login.php?url=admin/index.php");
    
    include_once '../dbUtility/User.php';
    $userId = $_SESSION['UID'];
    $userInfo = "";
    if($userId)
        $userInfo = getUserInfo($userId);
    
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Education Management System | Analytics System</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../style/style.css" />
        <link rel="stylesheet" href="../style/menu.css" />
        <script type="text/javascript" src="../js/jquery-1.9.1.js"></script>
        
        <!--[if lte IE 8]>
            <script type="text/javascript" src="../excanvas_r3/excanvas.js"></script>
        <![endif]-->
        
    </head>
    <body>
        <div class="header1">
            <div class="header1_top">
                <div class="header_top_logo">
                    <img src="../image/logo.png" class="img_logo" />
                </div>
                <div class="header_top_note">
                    <h2>
                        <?php
                            if(isset($userInfo) && $userInfo != "")
                            {
                                $userData = mysql_fetch_assoc($userInfo);
                                echo "<h2>";
                                echo "Welcom $userData[FirstName] $userData[LastName]"."<br/>";
                                echo "<a href='../logout.php'>Click to Log-off</a></h2>";
                            }
                            else{
                                echo "Welcome";
                            }
                            ?>
                    </h2>
                </div>
            </div>
            <div class="header1_nav">
                <ul class="mn-container">
                    <li><a href="#">Home</a></li>
                    <li style="width: 100px;"><!--<a href="#">Attendance</a>--></li>
                    <li style="margin-left: 100px;"><a href="../admin/newgenerateexam.php">Test</a></li>
                    <li style="margin-left: 100px;"><a href="../admin/newreport.php">Report</a></li>
                    <li style="width: 100px;"><!--<a href="#">Message</a>--></li>
                    <li STYLE="margin-left: 100px;"><a href="../admin/newuser.php">Users</a></li>
                </ul>
            </div>
        </div>
        

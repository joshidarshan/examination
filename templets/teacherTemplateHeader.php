<?php
/* if(!isset($_SESSION['UID']))
  header("Location: login.php");

  include_once '../dbUtility/User.php';
  $userId = $_SESSION['UID'];
  $userInfo = "";
  if($userId)
  $userInfo = getUserInfo($userId); */
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Education Management System | Analytics System</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../style/style.css" />
        <link rel="stylesheet" href="../style/menu.css" />
        <script type="text/javascript" src="../js/jquery-1.9.1.js"></script>
    </head>
    <body>
        <div class="header1">
            <div class="header1_top">
                <div class="header_top_logo">
                    <img src="../image/logo.png" class="img_logo" />
                </div>
                <div class="header_top_note">
                    <h2>
                        <?php
                        if (isset($userInfo) && $userInfo != "") {
                            $userData = mysql_fetch_assoc($userInfo);
                            echo "Welcom $userData[FirstName] $userData[LastName]";
                        }
                        else
                            echo "Welcome Guest";
                            ?>
                    </h2>
                </div>
            </div>
            <div class="header1_nav">
                <ul class="mn-container">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Attendance</a></li>
                    <li><a href="#">Test</a></li>
                    <li><a href="#">Report</a></li>
                    <li><a href="#">Message</a></li>
                    <li><a href="#">Users</a></li>
                </ul>
            </div>
        </div>

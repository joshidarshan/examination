<?php
include_once '../dbUtility/report.php';

$type = $_GET['type'];
$cls = $_GET['class'];
$dv = $_GET['div'];

if($type == 'classreport'){
    $data = GetStdStudentResult($cls, $dv);
    $returnData = "<table width='100%'>
                    <tr>
                        <th style='width: 400px; text-align: left;'>Student Name</th>
                        <th style='width: 200px; text-align: left;'>Test</th>
                        <th style='width: 200px; text-align: left;'>Score</th>
                    </tr>";
    
    foreach($data as $key=>$value){
        $returnData .= "<tr>
                            <td style='text-align: left;'><a href='reportdetail1.php?uid=".$value['Id']."&examId=".$value['ExamId']."'>".$value['Name']."</a></td>
                            <td style='text-align: left;'>".$value['ExamName']."</td>
                            <td style='text-align: left;'><strong>".$value['Mark']."  /  ".$value['TotalMark']."</strong></td>
                        </tr>";
    }
    $returnData .= "</table>";
    echo $returnData;
}

else if($type == 'users'){
    include_once '../dbUtility/User.php';
    $users = GetStdUsers($cls);
    $response = "";
    while ($rusers = mysql_fetch_assoc($users)){
        $response .= "<option value=$rusers[Id]>$rusers[FirstName] $rusers[LastName]</option>";
    }
    echo $response;
}
?>

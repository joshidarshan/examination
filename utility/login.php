<?php
    session_start();
    echo "Username: ".$_POST['userName']." Password: ".$_POST['password'];
    $url="";
    if(!empty($_GET))
        if(isset($_GET['url']))
            $url=$_GET['url'];
    
    include_once '../dbUtility/User.php';
    
    if($_POST['userName'] == "" && $_POST['password'] == "")
        header("Location: ../login.php");
    
    $username = $_POST['userName'];
    $password = $_POST['password'];
    
    if(isset($username) && isset($password)){
        $username = mysql_real_escape_string($username);
        $password = mysql_real_escape_string($password);
        
        $result = LogIn($username, $password);
        $data = mysql_fetch_assoc($result);
        if($data){
            $_SESSION['UID'] = $data['Id'];
            $_SESSION['Name'] = $data['FirstName']. " " .$data['LastName'];
            $_SESSION['Role'] = $data['Role'];
            
            if($data['Role'] == 'Admin')
                header("Location: ../admin/report.php");
            else
                header("Location: ../examListing.php");
            
        }
        else
            header("Location: ../login.php?error=1");
    }
?>
